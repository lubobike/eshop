<?php

class ErrorResponse
{
    private $error = '';
    private $message = '';

    function __construct($lab, $mess) {
       $this->error = $lab;
       $this->message = $mess;
   }
    
    function getJsonData(){
        return json_encode( get_object_vars($this) );
    }
}

