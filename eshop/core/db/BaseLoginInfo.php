<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BaseLoginInfo
 *
 * @author lubo
 */
class BaseLoginInfo {
   
    private static $servername = "localhost";
    private static $username="symmetry_lubo";
    private static $password="kYyexhwKnSJEQLmHY6zMc8C96VJ6kqbk";
    private static $database="symmetry_lubo";

    public static function createPdoConnection() {
        // Create connection
        try
        {
            return new PDO('mysql:dbname='.BaseLoginInfo::$database.
                    ';host='.BaseLoginInfo::$servername,
                    BaseLoginInfo::$username,
                    BaseLoginInfo::$password );
        }
        catch (PDOException $e)
        {
            die("Unable to connect: " . $e->getMessage());
        }  
    }
        
     public static function createConnection() {       
        $conn = new mysqli(BaseLoginInfo::$servername, BaseLoginInfo::$username, BaseLoginInfo::$password);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        $conn->select_db(BaseLoginInfo::$database);
        if ($conn->connect_error) {
            die("Database Selection failed: " . $conn->connect_error);
        } 
        
        $conn->set_charset("utf-8");
        
        return $conn; 
    }
    
}
