<?php

class BasicResponse
{
    protected $label = '';
    protected $message = '';

    function __construct($lab, $mess) {
       $this->label = $lab;
       $this->message = $mess;
   }
    
    function getJsonData(){
        return json_encode( get_object_vars($this) );
    }
}

class DataResponse extends BasicResponse
{
    private $data='';
    
    function __construct($lab, $mess, $dt) {
       parent::__construct($lab, $mess);        
       $this->data = $dt;
       
   } 
   
    function getJsonData(){
        return json_encode( get_object_vars($this) );
    } 
    
    function getData()
    {
        return $this->data;
    }
}

