<?php

function getJson() {
    $jsobj = json_decode($_POST['json']);
    if (json_last_error() == JSON_ERROR_NONE){
        return $jsobj;
    }
    return NULL;
}

function isLanguange($lang)
{
    if( ! is_string($lang )){
        return false;
    }
    
    $lang = trim($lang);

    if($lang != '') //Also tried this "if(strlen($strTemp) > 0)"
    {
         if (strlen($lang) == 2 ) return true;
         else return false;
    }

    return false;
}

function loadLangFromGet()
{
    if( isset($_GET['la'])) {
         $lang = $_GET['la']; 
         if(isLanguange($lang)) {
             return $lang;
         } else {
             return 'sk';
         }
    
    }
    return 'sk';
}

function strongEncr($input)
{
    return hash('sha256',$input);
}

function getProtocol()
{
    if(isset($_SERVER['HTTPS']))
    {
        if (! empty( $_SERVER["HTTPS"] )) 
        {
            return "https";
        }
    }
    
    return "http";
    
}

function getHostLink()
{
    return getProtocol().'://'.$_SERVER['SERVER_NAME'];
}

function imgBase64($data, $type)
{
   return 'data:image/' . $type . ';base64,' . base64_encode($data);
}

function getEncEmailFromLink()
{
    return $_GET['c'];
}

function setSession($key, $val)
{
    session_start();
    $_SESSION[ $key ] = $val;
}

function getSession($key)
{
    session_start();
    return $_SESSION[ $key ];
}

function checkSession()
{
    session_start();
    $key = $_GET['ci'];
    return $_SESSION[ $key ];
}

function clearSession()
{
    session_start();
    $key = $_GET['ci'];
    unset($_SESSION[ $key ]); //= NULL;
}
