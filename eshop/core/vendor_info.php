<?php
require_once('filters.php'); 
require_once('LanguageMessages.php'); 
require_once('BasicResponse.php'); 
require_once('ErrorResponse.php'); 
require_once('db/BaseLoginInfo.php'); 

class VendorInfo 
{
    private $email = '';
    private $password = '';
    public $error = '';
    private $ip_address ='';
    public $emailenc = '';
    public $lang = '';
    private $langMessages;
    
    public function loadFromWizardLink() {
        
        $conn = BaseLoginInfo::createConnection(); 
        
        if ($stmt = $conn->prepare("SELECT v_lang, v_email, v_password FROM vendor_reg WHERE v_encemail = ? AND v_verified = 1;")){
 
            $this->emailenc = getEncEmailFromLink();
            $stmt->bind_param('s', $this->emailenc);
            $stmt->execute();
            $stmt->bind_result($this->lang, $this->email, $this->password );
            if(!$stmt->fetch()){
                $this->error = "Login Error";
            }
            $this->langMessages = new LanguageMessages($this->lang);
    
        } else { $this->error = $conn->error;  }

        $conn->close();       
    }
    
    public function doesEmailExist()
    {
         $conn = BaseLoginInfo::createConnection(); 
        
        if ($stmt = $conn->prepare("SELECT COUNT(*) FROM vendor_reg WHERE v_email = ?;")){
 
            $stmt->bind_param('s', $this->email);
            $stmt->execute();
            $stmt->bind_result($count );
            if(!$stmt->fetch()){
                $this->error = "X6";
            }
    
        } else { 
            $this->error = $conn->error;  
            return TRUE;
        }

        $conn->close(); 
        
        if($count > 0) {
            $this->error = $this->langMessages->vregEmailExist;
            return TRUE;
        }
        return FALSE;
    }
    
    public function loginCheck()
    {
        $paramarray = getJson();
        if( $paramarray == NULL) {
            return FALSE;
        }
        
        $vemail = '';
        $vpassw = '';
        
        foreach ($paramarray as &$item) {
            if( $item->name == 'vemail') {$vemail = $item->value;}
            if( $item->name == 'vpassw') {$vpassw = $item->value;}
        }  
        
        if($this->email !== $vemail ||  $this->password !== strongEncr($vpassw) 
                || $this->emailenc !== strongEncr($vemail) ) {
            $this->error = $this->langMessages->loginFailed;
            return FALSE;        
        }
       
        return TRUE;
    }
    
    public function loadFromPost() {
        
        $paramarray = getJson();
        if( $paramarray == NULL) {return;}
        
        $emailreg = '';
        $emailcp = '';
        $passwf = '';
        $passws = '';      
        
        foreach ($paramarray as &$item) {
            if( $item->name == 'emailreg') {$emailreg = $item->value;}
            if( $item->name == 'emailcp') {$emailcp = $item->value;}
            if( $item->name == 'passwf') {$passwf = $item->value;}
            if( $item->name == 'passws') {$passws = $item->value;}
        }
        
        if($emailreg === $emailcp) {$this->email = $emailreg;}
        if($passwf === $passws) {$this->password = strongEncr($passwf);}
        $this->ip_address = $_SERVER['REMOTE_ADDR'];
        $this->emailenc = strongEncr($this->email);
        $this->lang = loadLangFromGet();
        $this->langMessages = new LanguageMessages($this->lang);
 
    }
    
    function saveRecord()
    {
       $conn = BaseLoginInfo::createConnection();

        if ($stmt = $conn->prepare("INSERT INTO vendor_reg (v_email , v_password, v_ip_address , v_encemail, v_lang, v_expiration_date) VALUES (?,?,?,?,?, NOW()+INTERVAL 5 YEAR);")){
            $stmt->bind_param('sssss', $this->email, $this->password , $this->ip_address, $this->emailenc, $this->lang );
            $stmt->execute();  
        } else { $this->error = "error insert (" . $conn->errno . ") " . $conn->error;  }

        $conn->commit();
        $conn->close();        
    }
    
    function sendEmail()
    {
        $to      =  $this->email; 
        $subject = $this->langMessages->vregEmailSubject;
        $headers = 'From: '. $this->langMessages->vregEmailFrom.' <'.$this->langMessages->vregresponseEmail.'>' . "\r\n" . 
        'Reply-To: '. $this->langMessages->vregEmailFrom.' <'.$this->langMessages->vregresponseEmail.'>' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        $message = '<html lang="'.$this->email.'"><body><div>'. $this->langMessages->vregEmailDivText.'</div>';
        $message .='<div><a href="'.getHostLink().'/eshop/reg/regwiz.php?v='.$this->emailenc
                .'">'. $this->langMessages->vregEmailLinkText .'</a></div>';
        $message .='</body></html>';

        mail($to, $subject, $message, $headers);       
    }
    
    function getJsonResponse(){
        $resp = new BasicResponse($this->langMessages->thanks.'!',$this->langMessages->vendorRegistrationSuccess.'!');
        return $resp->getJsonData();
    }
    
    function getJsonLoginOk(){
        $resp = new DataResponse(
                $this->langMessages->logingok.'!',
                $this->langMessages->loginwelcome.'!',
                strongEncr($this->email.'#'. time())
                );
        return $resp;
    }    
    
    function getJsonError() {
        $resp = new ErrorResponse($this->langMessages->jqerror.'!',$this->error.'!');
        return $resp->getJsonData();
    }
}


