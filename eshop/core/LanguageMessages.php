<?php
require_once('filters.php'); 

class LanguageMessages
{ 
    public $thanks ;
    public $enterEmail ;
    public $copyEmail ;
    public $passwordFirst ;
    public $passwordSecond; 
    public $vendorWantRegister ;
    public $registryVendor ;
    public $pleaseWait ;
    public $close ;
    public $registerConfirmation;
    public $serverNotResponding;
    public $jqerror;
    public $vendorRegistrationSuccess;
    public $invalidEmail ;
    public $shortLongPassword ;
    public $emptyEmail ;
    public $emailDoNotMatch ;
    public $emptyPassword ;
    public $passwordDoNotMatch ;
    public $emptyEmail2 ;
    public $emptyPassword2 ;
    
    public $vregresponseEmail;    
    public $vregEmailSubject;
    public $vregEmailFrom;
    public $vregEmailDivText;
    public $vregEmailLinkText;
    public $vregEmailExist;
    public $vendorEshopSection;
    public $vendorLoginButton;
    public $logingok;
    public $loginwelcome;
    public $loginFailed;
 /*
    public $; 
 */
    
    public function __construct($lang= 'NA') { 
        
        if($lang == 'NA' ) $lang = loadLangFromGet();
        
        switch($lang) {
            case 'sk': $arr = parse_ini_file ('langs/Slovak.ini'); break;
            case 'en': $arr = parse_ini_file ('langs/English.ini'); break;
            default: $arr = parse_ini_file ('langs/Slovak.ini'); break;
        }
        foreach ($arr as $key => $value)
        {
            $this->$key = $value;
        }
    } 
   
/*
  public static function getInstance($lang = 'NA')
  {
    if($lang == 'NA' ) $lang = loadLangFromGet();
    
    if ( is_null( self::$instance ) )
    {
      self::$instance = new self($lang);
    }
    return self::$instance;
  } 
 * 
 */  
}



