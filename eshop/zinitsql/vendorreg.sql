
drop table if exists vendor_reg;

CREATE TABLE `vendor_reg` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `v_email` varchar(128) NOT NULL,
  `v_password` varchar(64) NOT NULL,
  `v_encemail` varchar(64) NOT NULL,
  `v_verified` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `v_reset_request` smallint(6) unsigned NOT NULL DEFAULT '0',
  `v_ip_address` varchar(16) NOT NULL DEFAULT '',
  `v_createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `v_level` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `v_lang` varchar(4) NOT NULL DEFAULT 'en',
  `v_expiration_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `v_email` (`v_email`),
  KEY `vendor_reg_idx1` (`v_encemail`),
  KEY `vendor_reg_idx2` (`v_createDate`),
  KEY `vendor_reg_idx3` (`v_ip_address`),
  KEY `vendor_reg_idx4` (`v_verified`),
  KEY `vendor_reg_idx5` (`v_reset_request`),
  KEY `vendor_reg_idx6` (`v_level`),
  KEY `vendor_reg_idx7` (`v_lang`),
  KEY `vendor_reg_idx8` (`v_expiration_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;