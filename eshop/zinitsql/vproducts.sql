
drop table if exists ven_products;

CREATE TABLE `ven_products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `v_id` bigint(20) NOT NULL,
  `p_id` bigint(20) NOT NULL,
  `p_name` varchar(64) NOT NULL,
  `p_value` varchar(64) NOT NULL DEFAULT '',
  `p_spec` mediumtext,
  `p_lang` varchar(4) DEFAULT 'en',
  `p_createDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `p_startDay` datetime DEFAULT NULL,
  `p_endDay` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `ven_products_idx1` (`v_id`),
  KEY `ven_products_idx2` (`p_id`),
  KEY `ven_products_idx3` (`p_name`),
  KEY `ven_products_idx4` (`p_spec`(1)),
  KEY `ven_products_idx5` (`p_value`),
  KEY `ven_products_idx6` (`p_lang`),
  KEY `ven_products_idx7` (`p_createDate`),
  KEY `ven_products_idx8` (`p_startDay`),
  KEY `ven_products_idx9` (`p_endDay`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;