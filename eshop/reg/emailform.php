<?php
require_once('../core/filters.php'); 
require_once('../core/LanguageMessages.php');

$lang = loadLangFromGet(); 

$langMessages = new LanguageMessages($lang);

?><!DOCTYPE html>
<html lang="<?php echo $lang ?>">
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Eshop</title>
    <link href="../bootstrap3/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bootstrap3/css/bootstrap-theme.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../jq2/bootstrapValidator.min.css">
    <script>
    var lang='<?php echo $lang ?>';
    var serverNotResponding = '<?php echo $langMessages->serverNotResponding ?>';
    var jqerror  = '<?php echo $langMessages->jqerror ?>';
    var invalidEmail  = '<?php echo $langMessages->invalidEmail ?>';
    var shortLongPassword   = '<?php echo $langMessages->shortLongPassword ?>';
    var emptyEmail  = '<?php echo $langMessages->emptyEmail ?>';
    var emailDoNotMatch  = '<?php echo $langMessages->emailDoNotMatch ?>';
    var emptyPassword  = '<?php echo $langMessages->emptyPassword ?>';
    var passwordDoNotMatch  = '<?php echo $langMessages->passwordDoNotMatch ?>';
    var emptyEmail2  = '<?php echo $langMessages->emptyEmail2 ?>';
    var emptyPassword2 = '<?php echo $langMessages->emptyPassword2 ?>';
    </script>
    </head>
  <body>
 <!-- Modal -->
<div class="modal" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" >.......<?php echo $langMessages->pleaseWait ?>........</h4>
      </div>
      <div class="modal-body" >
       <img src="../img/ajax-loader.gif"> 
      </div>
    </div>
  </div>
</div>	     
<!-- Modal -->
<div class="modal" id="validatorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="statusModalLabel"></h4>
      </div>
      <div class="modal-body" id="modalmessage">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $langMessages->close ?></button>
      </div>
    </div>
  </div>
</div>	      
   <div class="container-fluid">
      <div class="row" >
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <br>
        <a href="?la=sk"><img src="../img/flagsk.jpg" ></a>
        <a href="?la=en"><img src="../img/flaguk.jpg" ></a>
            <div  id="formdiv">
           <h3 class="page-header"><?php echo $langMessages->vendorWantRegister ?></h3>
            <form  class="form-stat" id='regnew'>
            <div class="row">
            <div class="col-md-3">
                <div class="form-group">
            	<input type='email' name='emailreg' id='emailreg' class="form-control" placeholder="<?php echo $langMessages->enterEmail ?>">
                </div>
                <div class="form-group">
            	<input type='email' name='emailcp' id='emailcp' class="form-control" placeholder="<?php echo $langMessages->copyEmail ?>">
                </div>
                <div class="form-group">
            	<input type='password' name='passwf' id='passwf' class="form-control" placeholder="<?php echo $langMessages->passwordFirst ?>">
                </div>
                <div class="form-group">
            	<input type='password' name='passws' id='passws' class="form-control" placeholder="<?php echo $langMessages->passwordSecond ?>">
                </div>
            	<button id="submit" name="submit" type="submit" disabled class="btn btn-success"><?php echo $langMessages->registryVendor ?></button>     
            	<br>            
            </div>
            </div>       
            </form>
            </div>
 	<div id="regfinished" class="alert alert-danger hide" role="alert" >
        <?php echo $langMessages->registerConfirmation ?>    
	</div>	
        </div>
   </div>
  </div>
    <script src="../jq2/jquery.min.js"></script>
    <script src="../bootstrap3/js/bootstrap.min.js"></script>
    <script src="../jq2/bootstrapValidator.min.js" type="text/javascript" charset="utf-8" ></script> 
     <script src="newemailreg.js"></script>
  </body>
</html>