<?php
require_once('../core/vendor_info.php'); 
header("Content-type: application/json; charset=utf-8");

$vendor = new VendorInfo();
$vendor->loadFromPost();

if( ! $vendor->doesEmailExist() ) {
    $vendor->saveRecord();
    $vendor->sendEmail();
    echo $vendor->getJsonResponse();
} else {
    echo $vendor->getJsonError();
}