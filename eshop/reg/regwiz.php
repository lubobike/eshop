<!DOCTYPE html>
<html lang="sk">
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head><body>
<?php

require_once('../core/db/BaseLoginInfo.php'); 

$conn = BaseLoginInfo::createConnection();

$wiz = $_GET['v'];

if ($stmt = $conn->prepare("UPDATE vendor_reg SET v_verified=1, v_level=9  WHERE v_encemail=?;")){
    $stmt->bind_param('s', $wiz);
    $stmt->execute();  
} else { echo "error UPDATE (" . $conn->errno . ") " . $conn->error;  }

$conn->commit();

// create a prepared statement
if ($stmt = $conn->prepare("SELECT  v_ip_address, v_email FROM vendor_reg WHERE v_verified=1 AND v_encemail=?;")){
 
    $stmt->bind_param('s', $wiz);
    $stmt->execute();
    $stmt->bind_result($v_ip_address, $v_email);
    while ($stmt->fetch()) {
        printf("Registered %s  from IP Address %s </br>\n",  $v_email, $v_ip_address); // */
    }
    
} else { echo "error select (" . $conn->errno . ") " . $conn->error;  }


$conn->close();

?>
</body></html>

