
$( document ).ready(function() {

        $('#regfinished').hide();
	$('#regfinished').removeClass('hide');
    
        function showLoading()
	{
		$('#loadingModal').modal('show') ;	
	}
	function hideLoading()
	{
		$('#loadingModal').modal('hide') ;	
	}
	
	function showStatus(bigLabel, message)
	{
		$('#statusModalLabel').html(bigLabel);
		$('#modalmessage').html(message);
		$('#validatorModal').modal('show') ;
	} 
        
        $( document ).ajaxError(function(event, jqxhr, settings, exception) {
		
            hideLoading();
           var errorMessage = "N/A";
            try {
	        var json = $.parseJSON(jqxhr.responseText);
	        errorMessage = json.error;
	    } catch (e) { 
	    	errorMessage = serverNotResponding;
	    }
	    
            showStatus( jqerror+"!" , errorMessage );

	}); 
        
        $('#regnew').bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            onError: function (e, data) {
                $('#submit').prop('disabled', true);
            },
            onSuccess: function (e, data) {
                $('#submit').prop('disabled', false);
            },
             
            fields: {
                emailreg: {
                    validators: {
                        emailAddress: {
                            message: invalidEmail
                        }
                    }
                },
                emailcp: {
                    validators: {
                        emailAddress: {
                            message: invalidEmail
                        }
                    }
                },
                passwf: {
                    validators: {
                        regexp: {

                            regexp: /^.{8,32}$/i,
                            message: shortLongPassword 
                        }
                    }
                } ,
                passws: {
                    validators: {
                        regexp: {
                            regexp: /^.{8,32}$/i,
                            message: shortLongPassword 
                        }
                    }
                }               
            }
        });       
    
 	$('#regnew').submit(function(event) {
		event.preventDefault();
                
                if($('#emailreg').val()== '' ){
                    showStatus( jqerror , emptyEmail );
                    return;
                }
                if($('#emailcp').val()==''){
                    showStatus( jqerror , emptyEmail2 );
                    return;
                }
                if(!($('#emailreg').val()== $('#emailcp').val())){
                    showStatus( jqerror , emailDoNotMatch );
                    return;
                }              
                if($('#passwf').val()== '' ){
                    showStatus( jqerror , emptyPassword );
                    return;
                } 
                 if( $('#passws').val()==''){
                    showStatus( jqerror , emptyPassword2 );
                    return;
                } 
                if(!($('#passwf').val()== $('#passws').val())){
                    showStatus( jqerror , passwordDoNotMatch );
                    return;
                }                 
                
		showLoading();
                
 		var authurl =  "ajaoreg.php?la=" + lang;		
		var frmJson = {json:JSON.stringify($('#regnew').serializeArray())};
		$.ajax({
                    url:  authurl ,
                    type: "POST",
                    dataType: "json",
		    data: frmJson,
		    success: function (data) {
		    	hideLoading();
                        if(data.error) {
                            showStatus( data.error , data.message );    
                        } else {
                            showStatus( data.label , data.message );                           
                            $('#formdiv').hide();
                            $('#regfinished').show();
                        }
		    }
		});	
	});
});


