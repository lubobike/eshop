<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
 
    <title>Payroll-Point Batch Diagnostics </title>
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../bootstrap/css/datepicker.css">
    <link rel="stylesheet" href="../bootstrap/css/watable.css">
    
 	<script src="../bootstrap/js/jquery-2.1.3.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="../bootstrap/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
 	<script src="../bootstrap/js/bootstrap-datepicker.js" type="text/javascript" charset="utf-8"></script> 
 	<script src="../bootstrap/js/jquery.watable.js" type="text/javascript" charset="utf-8"></script>
	<script src="../bootstrap/js/chunkChecker.js" type="text/javascript" charset="utf-8"></script>
  </head>

  <body role="document">
    <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
         <a class="navbar-brand" href="home.html">Home</a> 
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
             <li > <a  href="../logout.jsp">Logout</a></li>           
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

     <div class="container theme-showcase" role="main">

      <div class="jumbotron">
        <p><br><img src="../graphics/symmetry.jpg"> Payroll-Point Batch Chunk Diagnostics</p>
      </div>
      
      <div class="page-header">
        <h3>List of Running or Dead Batch Processes</h3>
      </div>     
      
     <div class="container">
     
<!-- Modal -->
<div class="modal" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" >.......Please Wait........</h4>
      </div>
      <div class="modal-body" >
       <img src="../bootstrap/img/ajax-loader.gif"> 
      </div>
    </div>
  </div>
</div>	     
     
<!-- Modal -->
<div class="modal" id="validatorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Error!</h4>
      </div>
      <div class="modal-body" id="modalmessage">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>	
			  

      	<div id="chunkStamp" style="width:auto"></div>
      	<br>
      	
      	<button class="btn btn-primary" type="button" id="refreshnow">Refresh Now</button>
 	 </div>
      
      
    </div>
 
  </body>
</html>
