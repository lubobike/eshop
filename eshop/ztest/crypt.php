<?php
require '../global/aes.php';
	
function urlsafe_b64encode($string)
{
  $data = base64_encode($string);
  $data = str_replace(array('+','/','='),array('-','_','.'),$data);
  return $data;
}
function urlsafe_b64decode($string)
{
  $data = str_replace(array('-','_','.'),array('+','/','='),$string);
  $mod4 = strlen($data) % 4;
  if ($mod4) {
    $data .= substr('====', $mod4);
  }
  return base64_decode($data);
}

function bs64_aes_crypt($input, $password)
{
	$td = mcrypt_module_open('tripledes', '', 'ecb', '');
	$iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	$key = "QBa3zfwL6gG5Z6j5FQvNmUqF";
	mcrypt_generic_init($td, $key, $iv);
	
	$input = AesCtr::encrypt($input, $password, 256);
	$enc = urlsafe_b64encode(mcrypt_generic($td,$input)); 
	
	mcrypt_generic_deinit($td);
	mcrypt_module_close($td);

	return $enc;
}

function bs64_aes_decrypt($input, $password)
{
	$td = mcrypt_module_open('tripledes', '', 'ecb', '');
	$iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	$key = "QBa3zfwL6gG5Z6j5FQvNmUqF";
	mcrypt_generic_init($td, $key, $iv);
	
	$input = mdecrypt_generic($td,urlsafe_b64decode($input)); 
	
	mcrypt_generic_deinit($td);
	mcrypt_module_close($td);	
	
	return  AesCtr::decrypt(rtrim($input, "\0"), $password, 256);
}
?> 