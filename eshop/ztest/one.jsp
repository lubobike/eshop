<%@ page language="java" pageEncoding="utf-8" import="com.symmetry.payrollpoint.core.types.batch.BatchSettings" %>
<%
String logpref = (String) request.getAttribute("log_message");
String alterMessage = "";
BatchSettings bs = (BatchSettings) request.getAttribute("batchSettings");
if(bs != null)
	alterMessage = 	(bs.getShow_ADP_codes() == 1)?"ADP": ((bs.getShow_MTC_codes()==1)?"MTC":"" );

String path = request.getContextPath();
String basePath = "";
String https = request.getHeader("x-https");
if (https != null)
	basePath = https + "://"+request.getServerName()+path+"/";
else
	basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Payroll Point | the right taxes the first time</title>
	
    <meta http-equiv="keywords" content="payroll, point, payrollpoint">
    <meta http-equiv="description" content="payrollpoint ">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    
    <link rel="stylesheet" href="../graphics/styles/green-bob.css" />
    <link rel="stylesheet" href="../graphics/styles/prp_stylesheet.css" />
    <link rel="stylesheet" href="../graphics/styles/main.css" />
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css" />
    <link href="../bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="../fineuploader/fineuploader.css" rel="stylesheet" type="text/css" />
 
  <script type="text/javascript">
  
  <% if(request.getParameter("uid")==null) {%>		       
  var urlparams = "?cids=<%=request.getParameter("cids") %>";
	var uid = "";
  <% } else { %>      
  var urlparams = "?cids=<%=request.getParameter("cids") %>&uid=<%= request.getParameter("uid") %>";
	var uid = "<%=request.getParameter("uid") %>";
  <% } %>    

  var cids = "<%=request.getParameter("cids") %>";
  var uploadUrl = "<%= basePath %>upf/uploader.html"+urlparams

<% if(request.getAttribute("verify_ID") == null) { %>	
	var verifyU = false;
<% } else { %>      
	var verifyU = true;
<% } %>  

var alter = <%= (bs!=null && (bs.getShow_ADP_codes() == 1  || bs.getShow_MTC_codes() == 1 ) )%>;
       
</script>    

</head>
  
<body  class="greenpoint" >
  
  
<table  class="topmainAA" >
  <tr>
    <td class="text">&nbsp;</td>
    <td class="text" id="logo_bkgd2">&nbsp;</td>
    <td class="text">&nbsp;</td>
    <td class="text">&nbsp;</td>
    <td class="text">&nbsp;</td>
  </tr>
  <tr>
    <td  class="text AAtext" ><img src="<%= basePath %>graphics/bob/banner_left.png"  class="bannerA"  /></td>
    <td  class="text ABtext"  id="logo_bkgd"><table  class="bannerHolder topLogo" >
      <tr>
        <td  class="topLogoA" >
        
<% if(request.getAttribute("isLogo") == null) { %>	
	
<% } else { %>
	<img src="../img/cobrand_logo.jpeg?cids=<%= request.getParameter("cids")  %>">
<% } %>       
        
        </td>
      </tr>
      <tr>
        <td><img src="<%= basePath %>graphics/bob/spacer.gif"  class="logoDown"  /></td>
      </tr>
    </table></td>
    <td  class="text AAtext" ><img src="<%= basePath %>graphics/bob/banner_logo_right.png"  class="bannerA"  /></td>
    <td   class="text bannerText"  id="banner"><table  class="bannerHolder bannerTable" >
      <tr>
        <td class="bannerTagline"><img src="<%= basePath %>graphics/bob/spacer.gif"  class="spacerBanner"  /></td>
        <td class="bannerTagline">the right taxes the first time</td>
      </tr>
      <tr>
        <td class="alignRight" ><img src="<%= basePath %>graphics/bob/spacer.gif" class="spacerRight"  /></td>
        <td class="alignRight" ><img src="<%= basePath %>graphics/bob/prp_logo.png" class="spacerLogoRight"  /></td>
      </tr>
    </table></td>
    <td class="text bannerRightA" ><img src="<%= basePath %>graphics/bob/banner_right.png" class="bannerLogoRightA"  /></td>
  </tr>
</table>  
    
 <table  class="topmainAA" >
  <tr>
    <td><img src="<%= basePath %>graphics/bob/spacer.gif" class="bigSpacer"  /></td>
  </tr>
  <tr>
    <td><img src="<%= basePath %>graphics/bob/body_top.png" class="bodyBigTop"  /></td>
  </tr>
</table>   
<table  class="topmainAA"  id="bodyframe">
<tr>
    <td class="leftSpacerA" ><img src="<%= basePath %>graphics/bob/spacer.gif"  class="leftSpacerAI"  /></td>
    <td  class="middleAllCenter" >
          
      <table  class="middleCenterTable" >
      <tr ><td  class="middleTopTwenty" >
<!-- Modal -->
<div class="modal" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" >.......Please Wait........</h4>
      </div>
      <div class="modal-body" >
       <img src="../bootstrap/img/ajax-loader.gif"> 
      </div>
    </div>
  </div>
</div>	     
     
<!-- Modal -->
<div class="modal" id="validatorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="statusModalLabel"></h4>
      </div>
      <div class="modal-body" id="modalmessage">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>	

<!-- Modal -->
<div class="modal" id="confirmDel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Project Delete Confirmation</h4>
      </div>
      <div class="modal-body" id="resultmessage">
        Are you sure to Delete Selected Projects ?
      </div>
      <div class="modal-footer">
     <button type="button" data-dismiss="modal" class="btn btn-danger" id="delete">Delete Selected Projects</button>
    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
     </div>
    </div>
  </div>
</div>	

<nav  id="topnav" class="navbar navbar-default hide" role="navigation">
  <div class="container-fluid">
   <!-- Brand and toggle get grouped for better mobile display -->
    <div class="collapse navbar-collapse" >	
		<ul class="nav navbar-nav">
        <li id="homenav" class="active"><a href="javascript:home()">Home</a></li>
        <li id="listpro"><a href="javascript:projectlist()">Project List</a></li>
        <li id="listprocess"><a href="javascript:processlist()">Process List</a></li>
        <li id="uploadnav" ><a href="javascript:uploadFile()">Upload New File</a></li>
<% if(request.getAttribute("verify_ID") != null) { %>	        
        <li><a href="javascript:logout()">Logout</a></li>  
<% } %>
        </ul>  
    </div>
    </div>
</nav>

	<div id="batchEnabled" class="alert alert-danger hide" role="alert" >
	Batch Processing is Disabled Now! New File Can be Uploaded, but Will be Processed Later.
	</div>		        
	<div id="licenseExpired" class="alert alert-danger hide" role="alert" >
	</div>	      
    <div id="welcomediv" class="container-fluid hide">
	<br><br><br>
      <div class="row">
      <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      	<div class="col-md-3">
    	<h1>Welcome!</h1>
    	</div>
    	</div>
      </div>
    </div>	
    
<div id="processlistdiv" class="container-fluid hide"><div class="row">
        <div id="proclist" style="width:auto"></div>
</div></div>  

<div id="projectDetailsdiv" class="container-fluid hide"><div class="row">
<table  class="table table-striped table-bordered table-condensed" id="outputFileTable"> 	
</table>      
<button type="button" class="btn btn-primary hide" id="restartBatchButton">Restart Batch Request</button> 
</div></div>  

<div id="processdetailsdiv" class="container-fluid hide"><div class="row">
 
      <table  class="table table-striped table-bordered table-condensed" > 
      <tr><td>File name: </td><td ><div id="processFileName"></div></td></tr>
      <tr><td>Create Time: </td><td ><div id="processCreateTime"></div> </td></tr>
      <tr><td>Status: </td><td ><div id="bstatus"></div></td></tr>
      <tr><td>Processed time: </td><td ><div id="proc_time"></div></td></tr>
      <tr><td>Valid Input Lines: </td><td><div id="input_lines"></div></td></tr>
      </table>
      
    <br> Validating:
<div  class="progress progress-striped active">
   <div  id="prepareBar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
    </div>
</div>    
       Geocode Location Processing:
 <div  class="progress progress-striped active">
   <div  id="geoBar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
    </div>
</div>      
      Tax Info Processing:
  <div  class="progress progress-striped active">
   <div  id="taxBar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
    </div>
</div>      

<% if(alterMessage!="") {%>
      <br/> <%= alterMessage %> Info Processing:
  <div  class="progress progress-striped active">
   <div  id="adpBar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
    </div>
</div> 
<% }  %> 
</div></div>     
   

<div id="projectlistdiv" class="container-fluid hide"><div class="row">
        <div id="filelist" style="width:auto"></div>
      	<br>
      	<button type="button" data-dismiss="modal" class="btn btn-danger" id="deletechecked">Delete Selected Projects</button>
</div></div>    
    
       <div id="uploadfilediv" class="container-fluid hide">
      <div class="row">
      		<div id="uploadCsvFile" ></div>
      		<div id="uploadFinishedMessage"></div>
<br><br>
<a class="help" href="JavaScript:popupWin('../list/input_file_help.html','help','650','850')">CSV File Fields Description</a>
      		
		</div>       
       </div>
    
    <div id="loginformdiv" class="container-fluid hide">
	<br><br><br>
      <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h2 class="page-header">Payroll-Point Batch Login</h2>
			<form  class="form-stat" id='logif' >
			<div class="row">
				<div class="col-md-3">
	            	<input type="text" name='j_username' id='j_username'  class="form-control" placeholder="User Name">
	 				<br>
	            	<input type='password' name='j_password' id='j_password' class="form-control" placeholder="Password">
	            	<br>
	            	<button id="submit" name="submit" type="submit"  class="btn btn-success">Submit</button>     
	            </div>
	         </div>       
	         </form>

            <br>
<% if( logpref != null) {
		if(logpref != null) {
			if(!logpref.equals("")) {
			
				out.write(logpref);
			
			} else { %>
	<a   href="../../prputils/reset/password.html">Forget Password?</a>
<% 			} %>
<% 		} else { %>
	<a   href="../../prputils/reset/password.html">Forget Password?</a>
<% 		} %>

<% } else { %>
	<a   href="../../prputils/reset/password.html">Forget Password?</a>
<% } %>	

 	 </div>
      
   </div>
</div>

      </td></tr>

 
		<tr class="middleTopCenter100" >
		<td class="middleTopCenter"><br></td>
		</tr>   
      </table>   
          
</td>
     <td class="bottomA"  ><img src="<%= basePath %>graphics/bob/spacer.gif"  class="spacerBottomLeftA"  /></td>
      
    
    
    <td  class="text bottomTextA" ><img src="<%= basePath %>graphics/bob/spacer.gif"  class="spacerBotttomLeftAA"  /></td>
  </tr>
  <tr><td colspan="80" class="alignCenter">    <div class="copyright" >© 2009 - <%= java.util.Calendar.getInstance().get(java.util.Calendar.YEAR) %> Symmetry Software, All Rights Reserved.
  <br> <a  class="help" href="JavaScript:popupWin('../list/disclaimer_help.html','help','250','450')">Important Information.</a>  </div>
  </td></tr>
</table>    
  
 <table  class="topmainAA" >
  <tr>
    <td><span class="text"><img src="<%= basePath %>graphics/bob/footer.png"  class="spacerBottomBig"  /></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>   
       
<script src="../bootstrap/js/jquery-1.11.1.min.js"></script>
<script src="../bootstrap/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
<script src="../fineuploader/js/header.js"></script>
<script src="../fineuploader/js/util.js"></script>
<script src="../fineuploader/js/features.js"></script>
<script src="../fineuploader/js/promise.js"></script>
<script src="../fineuploader/js/button.js"></script>
<script src="../fineuploader/js/ajax.requester.js"></script>
<script src="../fineuploader/js/deletefile.ajax.requester.js"></script>
<script src="../fineuploader/js/handler.base.js"></script>
<script src="../fineuploader/js/window.receive.message.js"></script>
<script src="../fineuploader/js/handler.form.js"></script>
<script src="../fineuploader/js/handler.xhr.js"></script>
<script src="../fineuploader/js/paste.js"></script>
<script src="../fineuploader/js/uploader.basic.js"></script>
<script src="../fineuploader/js/dnd.js"></script>
<script src="../fineuploader/js/uploader.js"></script>
<script src="../fineuploader/uploader-csv.js"></script>      
<script src="../bootstrap/js/jquery.watable.js" type="text/javascript" charset="utf-8"></script>
<script src="../bjq/one.js"></script>
 
  </body> 
</html>
