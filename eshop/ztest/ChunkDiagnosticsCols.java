package com.symmetry.payrollpoint.core.json.wa.batch;

import com.symmetry.payrollpoint.core.json.wa.WaCol;
//import com.symmetry.payrollpoint.core.json.wa.WaUniqueCol;

public class ChunkDiagnosticsCols {
	
//	private WaUniqueCol id = new WaUniqueCol(1,"number", "id");
	private WaCol app_name = new WaCol(1,"string", "Application Name");
	private WaCol param_file_name = new WaCol(2,"string", "Project Name");
	private WaCol param_file_modif = new WaCol(3,"date", "File Created");
	private WaCol param_cobrand = new WaCol(4,"string", "Cobrand Name");
	private WaCol restartcount = new WaCol(5,"number", "Restart Count");
	private WaCol chunk_stamp = new WaCol(6,"date", "Chunk Stamp");
	
	public WaCol getApp_name() {
		return app_name;
	}
	public void setApp_name(WaCol app_name) {
		this.app_name = app_name;
	}
	public WaCol getParam_file_name() {
		return param_file_name;
	}
	public void setParam_file_name(WaCol param_file_name) {
		this.param_file_name = param_file_name;
	}
	public WaCol getParam_file_modif() {
		return param_file_modif;
	}
	public void setParam_file_modif(WaCol param_file_modif) {
		this.param_file_modif = param_file_modif;
	}
	public WaCol getParam_cobrand() {
		return param_cobrand;
	}
	public void setParam_cobrand(WaCol param_cobrand) {
		this.param_cobrand = param_cobrand;
	}
	public WaCol getRestartcount() {
		return restartcount;
	}
	public void setRestartcount(WaCol restartcount) {
		this.restartcount = restartcount;
	}
	public WaCol getChunk_stamp() {
		return chunk_stamp;
	}
	public void setChunk_stamp(WaCol chunk_stamp) {
		this.chunk_stamp = chunk_stamp;
	}
	

	

}
