package com.symmetry.payrollpoint.core.json.wa.batch;

import java.util.List;


import flexjson.JSONSerializer;

public class ChunkDiagnosticsTable {
	
	private ChunkDiagnosticsCols cols = new ChunkDiagnosticsCols();
	private List<ChunkDiagnosticsRows> rows  ;
	
	public String toJson() {
		   return new JSONSerializer().exclude("*.class").deepSerialize(this);
	}

	public ChunkDiagnosticsCols getCols() {
		return cols;
	}

	public void setCols(ChunkDiagnosticsCols cols) {
		this.cols = cols;
	}

	public List<ChunkDiagnosticsRows> getRows() {
		return rows;
	}

	public void setRows(List<ChunkDiagnosticsRows> rows) {
		this.rows = rows;
	}


}
