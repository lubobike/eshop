<%@ page language="java" pageEncoding="utf-8" 
	import="java.net.URLDecoder,com.symmetry.payrollpoint.core.types.batch.CustomTemplate,com.symmetry.payrollpoint.core.types.batch.BatchSettings,java.util.*" %>
<%
List<CustomTemplate> custTempList = (List<CustomTemplate> ) request.getAttribute("templateList");
CustomTemplate currentTemplate = (CustomTemplate) request.getAttribute("currentTemplate");

BatchSettings batchSettings = (BatchSettings) request.getAttribute("batchSettings");
%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Payrollpoint Geocode Location Log and Statistics">
    <meta name="author" content="Lubo">

    <title>Payrollpoint Reports</title>

    <!-- Bootstrap core CSS -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bjq/datepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="../bjq/bootstrap-select.min.css">
    <link rel="stylesheet" href="../bjq/bootstrapValidator.min.css">
    <link rel="stylesheet" href="../bjq/bootstrap-select.min.css">
    
    <!-- Custom styles for this template -->
    <link href="../bjq/dashboard.css" rel="stylesheet">

  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
      	<jsp:include page="inc/boottop.jsp" /> 

      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
      	<jsp:include page="inc/sidemenu.jsp" /> 

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<img src="../graphics/symmetry.jpg">
 <h2 class="page-header">Payroll-Point Batch Template Builder for 
        <% if( request.getParameter("ci") != null) { %> User: <%= URLDecoder.decode( request.getParameter("ci"), "UTF-8")  %>
       <% }  else { %> 
         <%= request.getAttribute("principal") %>
       <% } %> </h2> 
<!-- Modal -->
<div class="modal" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" >.......Please Wait........</h4>
      </div>
      <div class="modal-body" >
       <img src="../bootstrap/img/ajax-loader.gif"> 
      </div>
    </div>
  </div>
</div>	     
     
<!-- Modal -->
<div class="modal" id="validatorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Error!</h4>
      </div>
      <div class="modal-body" id="modalmessage">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>	

<!-- Modal -->
<div class="modal" id="resultModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Result:</h4>
      </div>
      <div class="modal-body" id="resultmessage">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>	

<!-- Modal -->
<div class="modal" id="confirmDel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Template Delete Confirmation</h4>
      </div>
      <div class="modal-body" id="resultmessage">
        Are you sure to Delete Template ?
      </div>
      <div class="modal-footer">
     <button type="button" data-dismiss="modal" class="btn btn-danger" id="delete">Delete Template</button>
    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
     </div>
    </div>
  </div>
</div>	



<div id="editingStatus"> &nbsp; </div>
<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
   <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
		<form id="profileForm" class="navbar-form navbar-left" role="search">
  	<div class="form-group">
          <input type="text" id="newName" name="newName" class="form-control" placeholder="Template Name"  >
        </div>
        <button type="button" id="newTempl" class="btn btn-success">Save New Template</button>
      </form> 
    </div>
    <div class="collapse navbar-collapse" >
        
      <form class="navbar-form navbar-left">
        <div class="form-group">        
	   
	  <select id="templList" name="templList" class="selectpicker show-tick form-control" >   
	  	<option value="@none">Please Select Template</option>
	  <% if(custTempList != null) {
     	 	
     	 		for ( Iterator<CustomTemplate> it = custTempList.iterator(); it.hasNext();){
     	 			
     	 			CustomTemplate custT = it.next(); %>
	  
	  	<option value="<%= custT.getId() %>"><%= custT.getTemplateName() %></option>
	  	
	  		<% }} %>
	   </select>  
       </div>
 			<button type="button" id="loadTempl" class="btn btn-primary">Load</button>   
 			<button type="button" id="saveTempl" class="btn btn-success">Save</button>   
 			<button type="button" id="deleteTempl" class="btn btn-danger">Delete</button>   
 	</form>         
             	
	<br>
            	
     </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>     

<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
   <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <form class="navbar-form navbar-left">
        <div class="form-group">
 <select name="geoOption" id="geoOption" class="selectpicker show-tick form-control">
	  	<option value="homeAddr">Home Address</option>
		<option value="workAdd">Work Address</option>
	  	<option value="normhomeAddr">Normalized Home Address</option>
		<option value="normworkAdd">Normalized Work Address</option>
		<option value="homelatitude">Home Latitude</option>
		<option value="homelongitude">Home Longitude</option>
		<option value="worklatitude">Work Latitude</option>
		<option value="worklongitude">Work Longitude</option>
		<option value="homeaccuracy">Home Address Accuracy</option>
		<option value="workaccuracy">Work Address Accuracy</option>
<% if(batchSettings != null)
		if(batchSettings.getShow_ste_location_codes() == 1) {
%>
		<option value="homeStateCode">STE Home State Code</option>
		<option value="homeCountyCode">STE Home County Code</option>
		<option value="homeCityCode">STE Home City Code</option>
		<option value="homeMuniCode">STE Home Municipality Code</option>
		<option value="homeSchoolCode">STE Home School Code</option>

		<option value="workStateCode">STE Work State Code</option>
		<option value="workCountyCode">STE Work County Code</option>
		<option value="workCityCode">STE Work City Code</option>
		<option value="workMuniCode">STE Work Municipality Code</option>
		<option value="workSchoolCode">STE Work School Code</option>
<% } %>
		<option value="homeStateName">Home State Name</option>
		<option value="homeCountyName">Home County Name</option>
		<option value="homeCityName">Home City Name</option>
		<option value="homeSchoolName">Home School Name</option>
		<option value="homeMuniName">Home Municipality Name</option>

		<option value="workStateName">Work State Name</option>
		<option value="workCountyName">Work County Name</option>
		<option value="workCityName">Work City Name</option>
		<option value="workSchoolName">Work School Name</option>
		<option value="workMuniName">Work Municipality Name</option>


		<option value="homePSDcode">Home PSD Code</option>
		<option value="homePSDrate">Home PSD Rate</option>
		<option value="workPSDcode">Work PSD Code</option>
		<option value="workPSDrate">Work PSD Rate</option>
							</select></div>
 			<button type="button" id="addgeo" class="btn btn-primary">Add</button>        
        </form>           	
    </div>
    <div class="collapse navbar-collapse" >
        
   <% if( batchSettings.getBatch_custom_ste() == 1) {%>
      <form class="navbar-form navbar-left">
        <div class="form-group">        
	   
	  <select id="taxOption" name="taxOption" class="selectpicker show-tick form-control" >   
	  	<option value="symmetrytaxid">Symmetry Tax ID</option>
	  	<option value="taxDescription">Tax Description</option>
	  	<option value="taxRate">Tax Rate</option>
	  	<option value="limitNote">Limit Note</option>
	  	<option value="limitPeriod">Limit Period</option>
	  	<option value="paidBy">Paid By</option>
	  	<option value="residentTax">Resident Tax</option>
	   </select>  
       </div>
 			<button type="button" id="addtax" class="btn btn-primary">Add</button>   
 	</form>     
<% }  %>     
<% if( batchSettings.getShow_ADP_codes() == 1) {%>
     <form class="navbar-form navbar-left" >
     <div class="form-group"> 
 	  <select id="adpOption" name="adpOption"  class="selectpicker show-tick form-control" >   
	  	<option value="adpCode">ADP Code</option>
	  	<option value="adpState">ADP State</option>
	  	<option value="adpName">ADP Name</option>
	  	<option value="adpCounty">ADP County</option>
	  	<option value="adpContact">ADP Contact</option>
	  	<option value="adpResRate">ADP Resident Rate</option>
	  	<option value="adpNonresRate">ADP Non-resident Rate</option>
	  	<option value="adpCodeType">ADP Code Type</option>
	  	</select>
	 </div>
 	<button type="button" id="addAdp" class="btn btn-primary">Add</button>   
     </form>
<% } else if( batchSettings.getShow_MTC_codes() == 1) { %>
     <form class="navbar-form navbar-left" >
        <div class="form-group"> 
	  		<select  id="mtcOption" name="mtcOption"  class="selectpicker show-tick form-control" >   
	  			<option value="mtcCode">MTC Code</option>
	  			<option value="mtcDescription">MTC Description</option>
	  			<option value="mtcPayee">MTC Payee</option>
	  			<option value="mtcName">MTC Name</option>
	  			<option value="mtcPhone">MTC Phone</option>
	  			<option value="mtcAddress">MTC Address</option>
	  			<option value="mtcAddress2">MTC Address 2</option>
	  			<option value="mtcCity">MTC City</option>
	  			<option value="mtcState">MTC State</option>
	  			<option value="mtcZip">MTC Zip</option>
	   		</select>         
        </div>
 			<button type="button" id="addMtc" class="btn btn-primary">Add</button>   
     </form>
 <% } %>
 
     <form class="navbar-form">
     	<div class="form-group"> 
 			<button type="button" id="addSupp" class="btn btn-primary">Add Suplemental Data</button>   
         	<button type="reset" id="clearbu" class="btn btn-default">Clear</button>
 			</div>
	</form>
 
 
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>     	
      	
      <div id="csvcols" ></div>
 	 </div>
      
   </div>
</div>
 	<script src="../bootstrap/js/jquery-2.1.0.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="../bootstrap/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
  	 <script src="../bjq/bootstrapValidator.min.js" type="text/javascript" charset="utf-8" ></script> 
  	 <script src="../bjq/bootstrap-select.min.js" type="text/javascript" charset="utf-8" ></script> 
  	 <script src="../bootstrap/bootstrap3-typeahead.min.js" type="text/javascript" charset="utf-8"></script> 
  	 <script src="../bjq/allcustTempl.js" type="text/javascript" charset="utf-8" ></script> 

  </body>
</html>
