package com.symmetry.payrollpoint.core.json.wa;

public class WaUniqueCol {
	
	private int index;
	private String type;
	private String friendly;
	private boolean unique = true;
	private boolean hidden = true;
	
	public WaUniqueCol(int ind, String typ, String frie)
	{
		this.index = ind;
		this.type = typ;
		this.friendly = frie;
	}
	
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFriendly() {
		return friendly;
	}
	public void setFriendly(String friendly) {
		this.friendly = friendly;
	}

	public boolean isUnique() {
		return unique;
	}

	public void setUnique(boolean unique) {
		this.unique = unique;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

}
