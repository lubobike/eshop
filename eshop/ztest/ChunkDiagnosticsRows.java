package com.symmetry.payrollpoint.core.json.wa.batch;

import java.sql.Timestamp;

public class ChunkDiagnosticsRows {
	
	private String app_name = "N/A";
	private String param_file_name = "N/A";
	private Timestamp param_file_modif = new Timestamp(0);
	private String param_cobrand =  "N/A";
	private int restartcount = 0;
	private Timestamp chunk_stamp = new Timestamp(0);
	
	public String getApp_name() {
		return app_name;
	}
	public void setApp_name(String app_name) {
		this.app_name = app_name;
	}
	public String getParam_file_name() {
		return param_file_name;
	}
	public void setParam_file_name(String param_file_name) {
		this.param_file_name = param_file_name;
	}
	public String getParam_cobrand() {
		return param_cobrand;
	}
	public void setParam_cobrand(String param_cobrand) {
		this.param_cobrand = param_cobrand;
	}
	public int getRestartcount() {
		return restartcount;
	}
	public void setRestartcount(int restartcount) {
		this.restartcount = restartcount;
	}
	public Timestamp getParam_file_modif() {
		return param_file_modif;
	}
	public void setParam_file_modif(Timestamp param_file_modif) {
		this.param_file_modif = param_file_modif;
	}
	public Timestamp getChunk_stamp() {
		return chunk_stamp;
	}
	public void setChunk_stamp(Timestamp chunk_stamp) {
		this.chunk_stamp = chunk_stamp;
	}

}
