
var position = 1;
var columnNames = new Array();
var coulmnValues = new Array();
columnNames[1] = "ID";
coulmnValues[1] = "id";
var maxFields = 100;

$( document ).ready(function() {
	
    $('.typeahead').typeahead();
	var cobs = new Array();
   
    $.ajax({
		url: location.protocol + "//"+ location.hostname + ":" + location.port +"/prputils/jsonadmin/getCobrandList.html",
		type: "POST",
		dataType: "json",
		headers: {   "dojo-head": "1"  },
		success: function (data) {
			cobs = data;
       	    $("#cobrandtop").typeahead({ source:data });
	    }
    });	 	
	
	$('#templList').selectpicker();
	
    $('#profileForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	newName: {
                validators: {
                    regexp: {
                    	onError: function(e, data) {
                    	    $('#newTempl').prop('disabled', true);
                        },
                        onSuccess: function(e, data) {
                            $('#newTempl').prop('disabled', false);
                        },                    	
                        regexp: /^[a-z0-9\s]{1,32}$/i,
                        message: 'Error, Character Not Allowed!'
                    }
                }
            }
        }
    });
    
    $('#newTempl').prop('disabled', true);
    
	$( document ).ajaxError(function(event, jqxhr, settings, exception) {
		
		var errorMessage = "N/A";
		
		try {
	        var json = $.parseJSON(jqxhr.responseText);
	        errorMessage = json.error;
	    } catch (e) { 
	    	errorMessage = "Server is Not Responding! Please Try Again Later.";
	    }		
		
		$('#loadingModal').modal('hide') ;	
		$('#modalmessage').html(errorMessage);
		$('#validatorModal').modal('show') ;
		
	});	    
	
	
    function buildTemplateData()
    {
		var tocode1 = "";
		
		for (var i=1; i<=position; i++)
		{
			tocode1 += coulmnValues[i];
			if( i != position )	tocode1 += "@";
		}
		
		return tocode1;
    }
  
	function addTemplate()
	{
		if($('#newName').val() == "") {
			alert("Please Enter Valid Template Name!");
			return;
		}
		
		$('#loadingModal').modal('show') ;	
    	
        $.ajax({
			url:  location.protocol + "//"+ location.hostname + ":" + location.port +"/prputils/jsonadmin/saveNewBatchTemplate.html" ,
			type: "POST",
			data: $('#newName').val() +"#"+buildTemplateData()+"#" + uname + "#",
			headers: {   "dojo-head": "1"  },
			success: function (data) {
				
				var nice = data.split(":");
				if(nice[1] == "OK") {				
				
					$('#templList').append($('<option>', {
	                    value: nice[2],
	                    text: $('#newName').val()
	                }));
					$('#templList').selectpicker('refresh');
					$("#editingStatus").html( " &nbsp; ");

					$('#loadingModal').modal('hide') ;	
					$('#resultmessage').html("Template Saved!");
					$('#resultModal').modal('show') ;
				
				} else {
					
					$('#loadingModal').modal('hide') ;	
					$('#resultmessage').html(nice[2]);
					$('#resultModal').modal('show') ;
				}
		    }
        });	
        
	}	
	
	function nameField(value)
	{
		if(value == ("id")) return  "ID";   
		else if(value == ("homeAddr")) return "Home Address";
		else if(value == ("workAdd")) return "Work Address";
		else if(value == ("normhomeAddr")) return "Normalized Home Address";
		else if(value == ("normworkAdd")) return "Normalized Work Address";
		else if(value == ("homelatitude")) return "Home Latitude";
		else if(value == ("homelongitude")) return "Home Longitude";
		else if(value == ("worklatitude")) return "Work Latitude";
		else if(value == ("worklongitude")) return "Work Longitude";
		else if(value == ("homeaccuracy")) return "Home Address Accuracy";
		else if(value == ("workaccuracy")) return "Work Address Accuracy";
		else if(value == ("symmetrytaxid")) return "Symmetry Tax ID";
		else if(value == ("taxDescription")) return "Tax Description";
		else if(value == ("taxRate")) return "Tax Rate";
		else if(value == ("limitNote")) return "Limit Note"; 
		else if(value == ("limitPeriod")) return "Period";
		else if(value == ("paidBy")) return "Paid by";
		else if(value == ("residentTax")) return "Resident Tax";
		
		else if(value == ("homePSDcode")) return "Home PSD Code";
		else if(value == ("homePSDrate")) return "Home PSD Rate";
		else if(value == ("workPSDcode")) return "Work PSD Code";
		else if(value == ("workPSDrate")) return "Work PSD Rate";
		
		else if(value == ("homeStateCode")) return "STE Home State Code";
		else if(value == ("homeCountyCode")) return "STE Home County Code";
		else if(value == ("homeCityCode")) return "STE Home City Code";
		else if(value == ("homeMuniCode")) return "STE Home Municipality Code";
		else if(value == ("homeSchoolCode")) return "STE Home School Code";
	
		else if(value == ("workStateCode")) return "STE Work State Code";
		else if(value == ("workCountyCode")) return "STE Work County Code";
		else if(value == ("workCityCode")) return "STE Work City Code";
		else if(value == ("workMuniCode")) return "STE Work Municipality code";
		else if(value == ("workSchoolCode")) return "STE Work School Code";
		
		else if(value == ("adpCode")) return "ADP Code";
		else if(value == ("adpState")) return "ADP State";
		else if(value == ("adpName")) return "ADP Name";
		else if(value == ("adpCounty")) return "ADP County";
		else if(value == ("adpContact")) return "ADP Contact";
		else if(value == ("adpResRate")) return "ADP Resident Rate";
		else if(value == ("adpNonresRate")) return "ADP Non-resident Rate";
		else if(value == ("adpCodeType")) return "ADP Code Type";

		else if(value == ("mtcCode")) return "MTC Code";
		else if(value == ("mtcDescription")) return "MTC Description";
		else if(value == ("mtcPayee")) return "MTC Payee";
		else if(value == ("mtcName")) return "MTC Name";
		else if(value == ("mtcPhone")) return "MTC Phone";
		else if(value == ("mtcAddress")) return "MTC Address";
		else if(value == ("mtcAddress2")) return "MTC Address 2";
		else if(value == ("mtcCity")) return "MTC City";
		else if(value == ("mtcState")) return "MTC State";
		else if(value == ("mtcZip")) return "MTC Zip";
		
		
		else if(value == ("homeStateName")) return "Home State Name";
		else if(value == ("homeCountyName")) return "Home County Name";
		else if(value == ("homeCityName")) return "Home City Name";
		else if(value == ("homeMuniName")) return "Home Municipality Name";
		else if(value == ("homeSchoolName")) return "Home School Name";
	
		else if(value == ("workStateName")) return "Work State Name";
		else if(value == ("workCountyName")) return "Work County Name";
		else if(value == ("workCityName")) return "Work City Name";
		else if(value == ("workMuniName")) return "Work Municipality Name";
		else if(value == ("workSchoolName")) return "Work School Name";	
		else if(value == ("employeeData"))  return "Supplemental Data";	
		
		else return "";
	}	
	
	function load_template (template)
	{
	   	var nice = template.split("@");	
	   	
		position = 1;
		columnNames = new Array();
		coulmnValues = new Array();
		columnNames[1] = "ID";
		coulmnValues[1] = "id";	   	
	   	
	   	for(var pos=1; pos < nice.length; pos++ ){
	   		
	   		position++;
	
	   		coulmnValues[position] =  nice[pos];
	   		columnNames[position] =  nameField( nice[pos] );
	   		
	   	}

		showColumns();
	}	
	
	function replaceTemplate(act)
	{
		if($('#templList').val() == "@none") {
			alert("Please Select Valid Template Name!");
			return;
		}
		
		$('#loadingModal').modal('show') ;	
    	
        $.ajax({
			url:  location.protocol + "//"+ location.hostname + ":" + location.port +"/prputils/jsonadmin/replaceBatchTemplate.html" ,
			type: "POST",
			data: $( "#templList option:selected" ).text() +"#"+buildTemplateData() + "#"+act+"#"+$('#templList').val()+"#" + uname + "#",
			headers: {   "dojo-head": "1"  },
			success: function (data) {
				
				var nice = data.split(":");
				if(nice[1] == "deleted") {		
					
					$('#templList option[value="'+$('#templList').val()+'"]').remove();
					$('#templList').selectpicker('refresh');		
				
					$('#loadingModal').modal('hide') ;	
					$('#resultmessage').html(nice[2]);
					$('#resultModal').modal('show') ;
				
				} else if(nice[1] == "error")  {

					$('#loadingModal').modal('hide') ;	
					$('#resultmessage').html(nice[2]);
					$('#resultModal').modal('show') ;
					
				} else if(nice[1] == "saved")  {

					$('#loadingModal').modal('hide') ;	
					$('#resultmessage').html(nice[2]);
					$('#resultModal').modal('show') ;

					$("#editingStatus").html( " &nbsp; ");
					
				} else if(nice[1] == "loaded")  {
					
					load_template(nice[2]);

					$('#loadingModal').modal('hide') ;	
					$('#resultmessage').html("Template Loaded!");
					$('#resultModal').modal('show') ;

					$("#editingStatus").html( " &nbsp; ");
					
				}
			}
        });	
        
	}		
	
	function showColumns()  
	{
		var ret = "<table class=\"table table-striped\"><th><b>CSV Column Number</b></th><th><b>CSV Column Name</b></th><th></th>";
		
		for (var i=1; i<=this.position; i++)
		{

			ret = ret + "<tr><td>"+i+"</td>"
					+"<td>"+columnNames[i]+"</td>"
					+"<td>"+((i>1)?"<a  class=\"help\" href=\"JavaScript:removeItem("+i+")\">Remove Item</a>":"")+"</td>"				
					+"<td>"+((i>2)?"<a  class=\"help\" href=\"JavaScript:upItem("+i+")\">Move Up</a>":"")+"</td>"				
					+"<td>"+((i>1 && i<this.position)?"<a  class=\"help\" href=\"JavaScript:downItem("+i+")\">Move Down</a>":"")+"</td>"				
					+"</tr>";
		}
		
		$("#csvcols").html( ret+"</table>");;
	}
	 
	$('#addgeo').click(function () {
		
		position++;
		if(position == maxFields ) {
			alert("Maximum Fields Reached!");
			return;
		}
	 	
		coulmnValues[position] =  $('#geoOption').val();
		columnNames[position] =  $( "#geoOption option:selected" ).text();
		
		$("#editingStatus").html( " Changes are Not Saved!");
		
		showColumns();
	});	
	
	$('#addtax').click(function () {
		
		position++;
		if(position == maxFields ) {
			alert("Maximum Fields Reached!");
			return;
		}
	 	
		coulmnValues[position] =  $('#taxOption').val();
		columnNames[position] =  $( "#taxOption option:selected" ).text();

		$("#editingStatus").html( " Changes are Not Saved!");
		
		showColumns();
	});	
	
	$('#addAdp').click(function () {
		
		position++;
		if(position == maxFields ) {
			alert("Maximum Fields Reached!");
			return;
		}
	 	
		coulmnValues[position] =  $('#adpOption').val();
		columnNames[position] =  $( "#adpOption option:selected" ).text();

		$("#editingStatus").html( " Changes are Not Saved!");
		
		showColumns();
	});	
	
	$('#addMtc').click(function () {
		
		position++;
		if(position == maxFields ) {
			alert("Maximum Fields Reached!");
			return;
		}
	 	
		coulmnValues[position] =  $('#mtcOption').val();
		columnNames[position] =  $( "#mtcOption option:selected" ).text();
		
		$("#editingStatus").html( " Changes are Not Saved!");
		
		showColumns();
	});	
	
	$('#addSupp').click(function () {
		
		position++;
		if(position == maxFields ) {
			alert("Maximum Fields Reached!");
			return;
		}
	 	
		coulmnValues[position] =  "employeeData";
		columnNames[position] =  "Supplemental Data";
		
		$("#editingStatus").html( " Changes are Not Saved!");
		
		showColumns();
	});	
	
	$('#newTempl').click(function () {
		addTemplate();
	});	
	
	$('#clearbu').click(function () {
		
		position = 1;
		columnNames = new Array();
		coulmnValues = new Array();
		columnNames[1] = "ID";
		coulmnValues[1] = "id";		

		$("#editingStatus").html( " &nbsp; ");
		
		showColumns();
	});
	
	$('#saveTempl').click(function () {
		replaceTemplate("save");
	});
	
	$('#loadTempl').click(function () {
		replaceTemplate("load");
	});
		
	$('#deleteTempl').click(function () {
		$('#confirmDel').modal('show')
        .one('click', '#delete', function() {
        	replaceTemplate("delete");
        });
			
	});	
	
	window.removeItem = function(pos)
	{
		for (var i=pos; i< this.position; i++){
			
			this.columnNames[i] = this.columnNames[i+1];
			this.coulmnValues[i] = this.coulmnValues[i+1];
			
		}
		this.position--;
		
		showColumns();

		$("#editingStatus").html( " Changes are Not Saved!");
	};	
	
	window.upItem = function(pos)
	{
		tempColName = this.columnNames[pos-1];
		tempColVal  = this.coulmnValues[pos-1];
			
		this.columnNames[pos-1] = this.columnNames[pos];
		this.coulmnValues[pos-1] = this.coulmnValues[pos];
			
		this.columnNames[pos] = tempColName;
		this.coulmnValues[pos] = tempColVal;

		showColumns();

		$("#editingStatus").html( " Changes are Not Saved!");
	};	
	
	window.downItem = function(pos)
	{
		tempColName = this.columnNames[pos+1];
		tempColVal  = this.coulmnValues[pos+1];
			
		this.columnNames[pos+1] = this.columnNames[pos];
		this.coulmnValues[pos+1] = this.coulmnValues[pos];
			
		this.columnNames[pos] = tempColName;
		this.coulmnValues[pos] = tempColVal;
		
		showColumns();

		$("#editingStatus").html( " Changes are Not Saved!");
	};		
	
	showColumns();
	
	$('#getTopCobrand').click(function () {
		uname = $('#cobrandtop').val();
		if(  $.inArray(uname,  cobs) == -1 ) {
			alert("Cobrand not Selected!");
			return;
		}
		prm = "?ci="+encodeURIComponent($('#cobrandtop').val());
		$('#loadingModal').modal('show') ;		
		golink('alltemplatebuilder.html');

	});     

	$('#unselectUser').click(function () {
		uname = "";
		prm = "";
		$('#loadingModal').modal('show') ;		
		golink('alltemplatebuilder.html');
	});  	 
	
	if(uname == "") {
		$('#userSelectedText').html("No User Selected!");
	} else {
		$('#userSelectedText').html("User "+uname +" Selected!");
	}		
});    