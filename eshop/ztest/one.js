
function popupWin(fullURL, winName, winHeight, winWidth) {
   winStats = 'toolbar=no,location=no,directories=no,menubar=no,status=no,';
   winStats += 'scrollbars=no,resizable=yes,copyhistory=no,';
   if (navigator.appName.indexOf("Microsoft")>=0) {
      winStats = winStats + ',left=10,top=10,width=' + winWidth + ',height=' + winHeight;
    } else {
      winStats+=',screenX=10,screenY=10,width=' + winWidth + ',height=' + winHeight;
    }

	showSurvey=window.open( fullURL, winName, winStats);
	showSurvey.focus();
}

$( document ).ready(function() {
	
	var initStyles = false;
	var _timer = "";
	var timercheckDelay = 600000;
	
	var restartBatchUrl = "";
	var projectFileEncoded = "";
	
	function showLoading()
	{
		$('#loadingModal').modal('show') ;	
	}
	function hideLoading()
	{
		$('#loadingModal').modal('hide') ;	
	}
	
	function showStatus(bigLabel, message)
	{
		$('#statusModalLabel').html(bigLabel);
		$('#modalmessage').html(message);
		$('#validatorModal').modal('show') ;
	}
	
	$('#logif').submit(function(event) {
		
		showLoading();
		event.preventDefault();
  
		var authurl =  location.protocol + "//"+ location.hostname + ":" + location.port +"/prpbatchwi/ajalogin/ajauthsec.html";
		
		var frmJson = JSON.stringify($('#logif').serializeArray());
		userName = $('#j_username').val();
		
		$.ajax({
			url:  authurl ,
			type: "POST",
	        headers: {  'dojo-head':'1' },
	        dataType: "json",
		    data: frmJson,
		    success: function (data) {
		    	
		    	hideLoading();
				if(data.status) {
					showWelcomeDiv();
					if(_timer == "" ) {
						_timer = window.setInterval(checkStatus, timercheckDelay);
					}
				} else {
					showStatus("Login Failed!", data.error );
				}
		    }
		});	
	});
	
	function showProperLoginPage(logged)
	{
		if(verifyU && !logged) {
			showLoginDiv();
		} else {
			showWelcomeDiv();
		}

		if(!initStyles) {
			$('#loginformdiv').removeClass('hide');
			$('#welcomediv').removeClass('hide');
			$('#topnav').removeClass('hide');
			$('#batchEnabled').hide();
			$('#batchEnabled').removeClass('hide');
			$('#licenseExpired').hide();
			$('#licenseExpired').removeClass('hide');
			$('#uploadfilediv').hide();
			$('#uploadfilediv').removeClass('hide');
			$('#projectlistdiv').hide();
			$('#projectlistdiv').removeClass('hide');
			$('#processlistdiv').hide();
			$('#processlistdiv').removeClass('hide');
			$('#processdetailsdiv').hide();
			$('#processdetailsdiv').removeClass('hide');
			$('#projectDetailsdiv').hide();
			$('#projectDetailsdiv').removeClass('hide');
			$('#restartBatchButton').hide();
			$('#restartBatchButton').removeClass('hide');
			
			initStyles = true;
		}
		
		if(_timer == "" && logged) {
			_timer = window.setInterval(checkStatus, timercheckDelay);
		}
		
	}
	
	function logincheck() {
		
	    var urltable =  location.protocol + "//"+ location.hostname + ":" + location.port +"/prpbatchwi/upf/logich.html?cids="+cids;
		
		$.ajax({
			url:  urltable ,
			type: "POST",
	        headers: {  'dojo-head':'1' },
			success: function (data) {
				
				showProperLoginPage(true);
			}
		});	
	};	
	
	function checkStatus() {
		
	    var urltable =  location.protocol + "//"+ location.hostname + ":" + location.port +"/prpbatchwi/upf/stati.html?cids="+cids;
		
		$.ajax({
			url:  urltable ,
			type: "POST",
	        headers: {  'dojo-head':'1' },
	        dataType: "json",
			success: function (data) {
				
				if(data.appEnabled) {
					$('#batchEnabled').hide();
				} else {
					$('#batchEnabled').show();
				}
				if(data.license == "") {
					$('#licenseExpired').hide();
				} else {
					$('#licenseExpired').html(data.license);
					$('#licenseExpired').show();
				}
			}
		});	
	};	
	
	function showWelcomeDiv()
	{
		clearCheckProcessTimer();
		$('#loginformdiv').hide();
		$('#welcomediv').show();
		$('#topnav').show();
		$('#batchEnabled').hide();
		$('#licenseExpired').hide();
		$('#uploadfilediv').hide();
		$('#projectlistdiv').hide();
		$('#processlistdiv').hide();
		$('#processdetailsdiv').hide();
		$('#projectDetailsdiv').hide();
	}
	
	function showLoginDiv()
	{
		clearCheckProcessTimer();
		$('#loginformdiv').show();
		$('#welcomediv').hide();
		$('#topnav').hide();
		$('#batchEnabled').hide();
		$('#licenseExpired').hide();
		$('#uploadfilediv').hide();
		$('#projectlistdiv').hide();
		$('#processlistdiv').hide();
		$('#processdetailsdiv').hide();
		$('#projectDetailsdiv').hide();
	}	
	
	function showUploadFileDiv()
	{
		clearCheckProcessTimer();
		$('#loginformdiv').hide();
		$('#welcomediv').hide();
		$('#topnav').show();
		$('#uploadfilediv').show();
		$('#projectlistdiv').hide();
		$('#processlistdiv').hide();
		$('#processdetailsdiv').hide();
		$('#projectDetailsdiv').hide();
	}	
	
	function loadProjectList()
	{
		$('#loadingModal').modal('show') ;	
	    var urltable =  location.protocol + "//"+ location.hostname + ":" + location.port +"/prpbatchwi/upf/ajafilelist.html"+urlparams;
		
	    $.ajax({
			url:  urltable ,
			type: "POST",
			dataType: "json",
			success: function (data) {
	       		waTable.setData(data);
	    		$('#loadingModal').modal('hide') ;	
		    }
	    });			
		
	}
	
	function loadProcessList()
	{
		$('#loadingModal').modal('show') ;	
	    var urltable =  location.protocol + "//"+ location.hostname + ":" + location.port +"/prpbatchwi/upf/ajaproclist.html"+urlparams;
		
	    $.ajax({
			url:  urltable ,
			type: "POST",
			dataType: "json",
			success: function (data) {
				procTable.setData(data);
	    		$('#loadingModal').modal('hide') ;	
		    }
	    });			
		
	}	
	
	function showProjectListDiv()
	{
		clearCheckProcessTimer();
		$('#loginformdiv').hide();
		$('#welcomediv').hide();
		$('#topnav').show();
		$('#uploadfilediv').hide();
		$('#processlistdiv').hide();
		$('#processdetailsdiv').hide();
		$('#projectlistdiv').show();
		$('#projectDetailsdiv').hide();
		
		loadProjectList();
	}	

	function showProcessListdiv()
	{
		clearCheckProcessTimer();
		$('#loginformdiv').hide();
		$('#welcomediv').hide();
		$('#topnav').show();
		$('#uploadfilediv').hide();
		$('#projectlistdiv').hide();
		$('#processdetailsdiv').hide();
		$('#processlistdiv').show();
		$('#projectDetailsdiv').hide();
		
		loadProcessList();
	}	
	
	var bid_timer = "";
	var batchidparams = "";
	
	window.showProcessDetaildiv = function(bid)
	{
		$('#loginformdiv').hide();
		$('#welcomediv').hide();
		$('#topnav').show();
		$('#uploadfilediv').hide();
		$('#projectlistdiv').hide();
		$('#processlistdiv').hide();
		$('#processdetailsdiv').show();
		$('#projectDetailsdiv').hide();

		batchidparams = "?cids="+cids+"&uid="+uid+"&bid="+bid;
		var urltable =  location.protocol + "//"+ location.hostname + ":" + location.port +"/prpbatchwi/upf/bidinfo.html"+batchidparams;
		$.ajax({
			url:  urltable ,
			type: "POST",
		    headers: {  'dojo-head':'1' },
		    dataType: "json",
			success: function (data) {
				$('#processFileName').html( data.projectName );
				$('#processCreateTime').html( new Date( data.createdTime ));
			}
		});
		$('#proc_time').html("");
		$('#input_lines').html("");
		
		if(bid_timer == "") {
			getStatus();
			bid_timer = window.setInterval(getStatus, 2000); 	//this will update progress bars every 2 seconds
		}
		
	}	
	
	function showProjectDetaildiv(fn)
	{
		$('#loginformdiv').hide();
		$('#welcomediv').hide();
		$('#topnav').show();
		$('#uploadfilediv').hide();
		$('#projectlistdiv').hide();
		$('#processlistdiv').hide();
		$('#processdetailsdiv').hide();
		$('#projectDetailsdiv').show();
		
		$('#loadingModal').modal('show') ;	
	    var fileurl =  location.protocol + "//"+ location.hostname + ":" + location.port +"/prpbatchwi/upf/ajaprojfilelist.html"+urlparams+"&fn="+fn;
		
	    $.ajax({
			url:  fileurl ,
			type: "POST",
			dataType: "json",
			success: function (data) {
				
				$("#outputFileTable").html("");
				$("#outputFileTable").append("<tr><td>Project Name</td><td>" + data.projectName + "</td></tr>");
				
				var inputFile = false;
				var inputFileName = "";
				var inputFileLink = "";

				var notProcFile = false;
				var notProcFileName = "";
				var notProcFileLink = "";
				
				var taxFile = false;
				var taxFileName = "";
				var taxFileLink = "";
				
				var manaFile = false;
				var manaFileName = "";
				var manaFileLink = "";
				
				var exFile = false;
				var exFileName = "";
				var exFileLink = "";	

				var geoFile = false;
				var geoFileName = "";
				var geoFileLink = "";	
				
				var customfiles = new Array();
				var numbercustomfields = 0;
				
				data.fileNames.forEach(function(entry) {
					if(entry.order == 0) {
						inputFile = true;
						inputFileName = entry.fileName;
						inputFileLink = entry.encoded;
					}
					if(entry.order == 1) {
						notProcFile = true;
						notProcFileName = entry.fileName;
						notProcFileLink = entry.encoded;
					}	
					if(entry.order == 2) {
						taxFile = true;
						taxFileName = entry.fileName;
						taxFileLink = entry.encoded;
					}
					if(entry.order == 3) {
						manaFile = true;
						manaFileName = entry.fileName;
						manaFileLink = entry.encoded;
					}
					if(entry.order == 4) {
						exFile = true;
						exFileName = entry.fileName;
						exFileLink = entry.encoded;
					}	
					if(entry.order == 5) {
						geoFile = true;
						geoFileName = entry.fileName;
						geoFileLink = entry.encoded;
					}					
					if(entry.order == 6) {
						customfiles[ numbercustomfields++ ] = entry;
					}
				});
				
				if(inputFile) {
					$("#outputFileTable").append("<tr><td>Input File Link: </td><td><a target=\"_blank\" href=\"../file/" + inputFileName+"?fn="+inputFileLink+"&cids="+cids+"\" >"+inputFileName+"</a></td></tr>");
					$("#outputFileTable").append("<tr><td>Input File Create Date: </td><td>"+new Date(data.inputFileLastModified)+"</td></tr>");
				}
				if(notProcFile) {
					$("#outputFileTable").append("<tr><td>Not Processed Report File Link:</td><td><a target=\"_blank\" href=\"../file/" + notProcFileName+"?fn="+notProcFileLink+"&cids="+cids+"\" >"+notProcFileName+"</a></td></tr>");
				}	
				if(geoFile) {
					$("#outputFileTable").append("<tr><td>Geocode Report File Link: </td><td><a target=\"_blank\" href=\"../file/" + geoFileName+"?fn="+geoFileLink+"&cids="+cids+"\" >"+geoFileName+"</a></td></tr>");
				}	
				if(taxFile) {
					$("#outputFileTable").append("<tr><td>Tax Report File Link: </td><td><a target=\"_blank\" href=\"../file/" + taxFileName+"?fn="+taxFileLink+"&cids="+cids+"\" >"+taxFileName+"</a></td></tr>");
				}
				if(manaFile) {
					$("#outputFileTable").append("<tr><td>Management Report File Link: </td><td><a target=\"_blank\" href=\"../file/" + manaFileName+"?fn="+manaFileLink+"&cids="+cids+"\" >"+manaFileName+"</a></td></tr>");
				}
				if(exFile) {
					$("#outputFileTable").append("<tr><td>Exception Report File Link: </td><td><a target=\"_blank\" href=\"../file/" + exFileName+"?fn="+exFileLink+"&cids="+cids+"\" >"+exFileName+"</a></td></tr>");
				}
				if(numbercustomfields > 0) {
					$("#outputFileTable").append("<tr><td>Custom Section Templates: </td><td>"+
							"<select id=\"custlist\" name=\"custlist\" class=\"selectpicker show-tick form-control\" > "  
							+"<option value=\"_^none\">Please Select Template</option></select>"
							+"</tr><tr><td>Custom File Link: </td><td><span id=\"custfile\"></span></td></tr>");
					
					customfiles.forEach(function(entry) {
						$('#custlist').append($('<option>', {
							value: entry.encoded,
							text: entry.fileName
						}));
					});
					$('#outputFileTable').on('change', "#custlist" ,function() {
						var enc = $( "#custlist" ).val();
						var disp = $( "#custlist option:selected" ).text();
						
						if(enc != "_^none")
							$("#custfile").html( "<a  target=\"_blank\" href=\"../file/"+disp+"?cids="+cids+"&fn="+enc+uid+"\">" +disp+"</a>");
						else
							$("#custfile").html( "" );
					});					
				}
				$("#outputFileTable").append("<tr><td>Status: </td><td>"+data.status+"</td></tr>");
				if( data.batchRunningId != "") {
					$("#outputFileTable").append("<tr><td>Project is in Progress: </td><td><a href=\"JavaScript:window.showProcessDetaildiv('"+data.batchRunningId+"')\">Click Here for Details</a></td></tr>");
				}
				
				if( data.batchProcessed > data.requestForRestart) {
					$("#outputFileTable").append("<tr><td>Processed time: </td><td>" +new Date( data.processedTime) + "</td></tr>");
					$("#outputFileTable").append("<tr><td>Number of All Input Lines: </td><td>" + data.allInputLines + "</td></tr>");
					$("#outputFileTable").append("<tr><td>Valid Input Lines: </td><td>" + data.validInputLines + "</td></tr>");
					$("#outputFileTable").append("<tr><td>Processed rows: </td><td>" + data.processedRows + "</td></tr>");
					$("#outputFileTable").append("<tr><td>Not Processed rows: </td><td>" + data.notProcessedRows + "</td></tr>");
				}
				if(inputFile) {
					$("#outputFileTable").append("<tr><td>Batch Processed #: </td><td>" + data.batchProcessed + "</td></tr>");
					$("#outputFileTable").append("<tr><td>Request for restart #: </td><td>" + data.requestForRestart + "</td></tr>");
				}
				if(inputFile && data.batchProcessed > data.requestForRestart ) {
					restartBatchUrl = location.protocol + "//"+ location.hostname + ":" + location.port +"/prpbatchwi/upf/ajarestartbatch.html"+urlparams+"&fn="+fn+"&ifm="+data.inputFileLastModified;
					projectFileEncoded = fn;
					$('#restartBatchButton').show();
				} else if(inputFile && data.batchProcessed <= data.requestForRestart){
					$('#restartBatchButton').hide();
				} else {
					$('#restartBatchButton').hide();
				}
				
	    		$('#loadingModal').modal('hide') ;	
		    }
	    });			
		
	}
	
	$( document ).ajaxError(function(event, jqxhr, settings, exception) {
		
		var errorMessage = "N/A";
		try {
	        var json = $.parseJSON(jqxhr.responseText);
	        errorMessage = json.error;
	    } catch (e) { 
	    	errorMessage = "Server is Not Responding! Please Try Again Later.";
	    }
	    if(initStyles) {
	    	showStatus( "Error!" , errorMessage );
	    }
	    clearCheckProcessTimer();
	    showProperLoginPage(false);
	});	

	logincheck();
	
	window.logout = function()
	{
	    var urllogout =  location.protocol + "//"+ location.hostname + ":" + location.port +"/prpbatchwi/logout.jsp";
	    window.clearInterval(_timer);
	    clearCheckProcessTimer();
		_timer = "";					
		
		$.ajax({
			url:  urllogout ,
			type: "GET",
			success: function (data) {
				showProperLoginPage(false);
			}
		});			
	}
	
	function removeNavActiveClass()
	{
		$('#homenav').removeClass('active');
		$('#uploadnav').removeClass('active');
		$('#listpro').removeClass('active');
		$('#listprocess').removeClass('active');
	}

	window.home = function()
	{
		showWelcomeDiv();
		removeNavActiveClass();
		$('#homenav').addClass('active');
	}

	window.uploadFile = function()
	{
		showUploadFileDiv();
		removeNavActiveClass();
		$('#uploadnav').addClass('active');
	}
	
	window.projectlist = function()
	{
		showProjectListDiv();
		removeNavActiveClass();
		$('#listpro').addClass('active');
	}
	window.processlist = function()
	{
		showProcessListdiv();
		removeNavActiveClass();
		$('#listprocess').addClass('active');
	}	
	
	
	function  procDetails(data) {
	 	   if (typeof data.column != 'undefined') {
	 			showProcessDetaildiv(data.row.id);
	 			removeNavActiveClass();
	 	   }
	}
	
	function  projClick(data) {
	 	   if (typeof data.column != 'undefined') {
	 		  showProjectDetaildiv(data.row.id);
	 			removeNavActiveClass();
	 	   }
	} 

	var waTable = new createWaTable('#filelist' , projClick, true);
	var procTable = new createWaTable('#proclist', procDetails, false);

	function createWaTable(divid, rowCallback, chckbx ) {
	
		return $(divid).WATable({
	       debug:false,                 //Prints some debug info to console
	       pageSize: 10,                //Initial pagesize
	       filter: false,               //Show filter fields
	       sorting: true,              //Enable sorting
	       sortEmptyLast:true,         //Empty values will be shown last
	       columnPicker: false,         //Show the columnPicker button
	       pageSizes: [],  			//Set custom pageSizes. Leave empty array to hide button.
	       hidePagerOnEmpty: true,     //Removes the pager if data is empty.
	       checkboxes: chckbx,           //Make rows checkable. (Note. You need a column with the 'unique' property)
	       checkAllToggle:chckbx,        //Show the check-all toggle
	       preFill: true,              //Initially fills the table with empty rows (as many as the pagesize).
	       types: {                    //Following are some specific properties related to the data types
	           string: {
	               //filterTooltip: "Giggedi..."    //What to say in tooltip when hoovering filter fields. Set false to remove.
	               //placeHolder: "Type here..."    //What to say in placeholder filter fields. Set false for empty.
	           },
	           number: {
	               decimals: 1   //Sets decimal precision for float types
	           },
	           bool: {
	               //filterTooltip: false
	           },
	           date: {
	             utc: false,            //Show time as universal time, ie without timezones.
	             format: 'MMM/dd/yyyy h(:mm)TT',   //The format. See all possible formats here http://arshaw.com/xdate/#Formatting.
	             datePicker: true      //Requires "Datepicker for Bootstrap" plugin (http://www.eyecon.ro/bootstrap-datepicker).
	           }
	       },
	
	       rowClicked:   rowCallback    
	   }).data('WATable');  
	}
	
	function clearCheckProcessTimer()
	{
		if( bid_timer != "") {
			window.clearInterval(bid_timer);		
			bid_timer = "";
		}
	}
	
	function getStatus()
	{
  
		var urltable =  location.protocol + "//"+ location.hostname + ":" + location.port +"/prpbatchwi/upf/batchprogress.html"+batchidparams;
		
		$.ajax({
			url:  urltable ,
			type: "POST",
		    headers: {  'dojo-head':'1' },
		    dataType: "json",
			success: function (data) {
				
				var prepPerc = data.preparedPercent/10 ;
				var geoPerc = data.processedGeocodePercent/10;
				var taxPerc = data.processedTaxPercent/10;

				$('#prepareBar').css('width', prepPerc +'%').attr('aria-valuenow', prepPerc);
				$('#prepareBar').html( ''+ prepPerc+'%');

				$('#geoBar').css('width', geoPerc+'%').attr('aria-valuenow', geoPerc );
				$('#geoBar').html( ''+ geoPerc +'%');
				
				$('#taxBar').css('width', taxPerc+'%').attr('aria-valuenow', taxPerc);
				$('#taxBar').html( ''+ taxPerc +'%');
				
				if(alter) {
					var alterPerc = data.processedAlterPercent/10;
					$('#adpBar').css('width', alterPerc+'%').attr('aria-valuenow', alterPerc);
					$('#adpBar').html( ''+ alterPerc +'%');										
				}
				
				if(data.status == 5){
					clearCheckProcessTimer();
						//we are done, no need to check anymore
					$("#bstatus").html("Ready for pick up");	
					$("#proc_time").html(new Date(data.processTime));	
				} else {
					$("#input_lines").html(data.validInputLines);	
				}
				
				if(data.status == 0)
					$("#bstatus").html("Failed");	
				if(data.status == 3)
					$("#bstatus").html("Stopped");	
				if(data.status == 4)
					$("#bstatus").html("Running");					
				
		    }
		});	
	}	
	
	
	$('#deletechecked').click(function () {
		$('#confirmDel').modal('show')
        .one('click', '#delete', function() {

        	$('#loadingModal').modal('show') ;	
            $.ajax({
    			url:  location.protocol + "//"+ location.hostname + ":" + location.port +"/prpbatchwi/upf/ajadeleteProjects.html"+urlparams ,
    			type: "POST",
    			dataType: "json",
    			data: waTable.getData(true),
    			headers: {   "dojo-head": "1"  },
    			success: function (data) {  
    	       		waTable.setData(data);
    	    		$('#loadingModal').modal('hide') ;	
    	    		$('#myModalLabel').html("Result:");
    	    		$('#modalmessage').html("Project(s) Deleted!");
    	    		$('#validatorModal').modal('show') ;
    	    		
    			}
            });
        });
			
	});		
	
	$('#restartBatchButton').click(function () {
		
		if(restartBatchUrl != "") {

	       	$('#loadingModal').modal('show') ;	
            $.ajax({
    			url:  restartBatchUrl,
    			type: "POST",
    			dataType: "json",
    			headers: {   "dojo-head": "1"  },
    			success: function (data) {  

    	    		$('#loadingModal').modal('hide') ;	
    				$('#restartBatchButton').hide();
    				showProjectDetaildiv(projectFileEncoded);
    			}
            });			
			
		}
		
		
	});
	
});