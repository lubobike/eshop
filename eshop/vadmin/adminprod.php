<?php
require_once('../core/vendor_info.php'); 
require_once('../core/LanguageMessages.php');

$vendor = new VendorInfo();
$vendor->loadFromWizardLink();

$langMessages = new LanguageMessages($vendor->lang);

?><!DOCTYPE html>
<html lang="<?php echo $vendor->lang ?>">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Eshop Admin</title>
    <link href="../bootstrap3/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bootstrap3/css/bootstrap-theme.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../jq2/datepicker.css">
    <link rel="stylesheet" href="../jq2/watable.css">
    <link href="../jasny/css/jasny-bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="../jq2/navgray.css">
    <script>
    var c='<?php echo $_GET['c']; ?>';
    var serverNotResponding = '<?php echo $langMessages->serverNotResponding ?>';
    var jqerror  = '<?php echo $langMessages->jqerror ?>';
    var invalidEmail  = '<?php echo $langMessages->invalidEmail ?>';
    var shortLongPassword   = '<?php echo $langMessages->shortLongPassword ?>';
    var emptyEmail  = '<?php echo $langMessages->emptyEmail ?>';
    var emailDoNotMatch  = '<?php echo $langMessages->emailDoNotMatch ?>';
    var emptyPassword  = '<?php echo $langMessages->emptyPassword ?>';
    </script>    
  </head>
  <body>
<!-- Modal -->
<div class="modal" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" >.......<?php echo $langMessages->pleaseWait ?>........</h4>
      </div>
      <div class="modal-body" >
       <img src="../img/ajax-loader.gif"> 
      </div>
    </div>
  </div>
</div>	     
<!-- Modal -->
<div class="modal" id="validatorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="statusModalLabel"></h4>
      </div>
      <div class="modal-body" id="modalmessage">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $langMessages->close ?></button>
      </div>
    </div>
  </div>
</div>	      

<div class="container-fluid">

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <br>
            <div  id="logdiv">
           <h3 class="page-header"><?php echo $langMessages->vendorEshopSection ?></h3>
            <div id="logwelcome" class="alert alert-success" role="alert" >Please Login</div>
            <div id="logdanger" class="alert alert-danger hide" role="alert" > </div>  
           <form  class="form-stat" id='loginf'>
            <div class="row">
            <div class="col-md-3">
                <div class="form-group">
            	<input type='email' name='vemail' id='vemail' class="form-control" placeholder="<?php echo $langMessages->enterEmail ?>">
                </div>
                <div class="form-group">
            	<input type='password' name='vpassw' id='vpassw' class="form-control" placeholder="<?php echo $langMessages->passwordFirst ?>">
                </div>
            	<button id="submit" name="submit" type="submit" disabled class="btn btn-success"><?php echo $langMessages->vendorLoginButton ?></button>     
            	<br>            
            </div>
            </div>       
            </form>
            </div>
        <div id="mainMenu" class="hide">
  
 
      <div class="masthead">
        <nav>
          <ul class="nav nav-justified">
            <li id="searchPmenu" class="active"><a href="javascript:searchProduct()">Search</a></li>
            <li id="addPmenu" ><a href="javascript:addProduct()">Add Product</a></li>
            <li id="editPmenu"><a href="javascript:editProfile()">Edit Profile</a></li>
            <li id="addFmenu"><a href="javascript:feddbackForm()">Contact</a></li>
            <li><a href="javascript:vlogout()">Logout</a></li>
          </ul>
        </nav>
      </div>    
        </div>
        <br>
         <div id="searchProd" class="hide">  
 <div class="container-fluid">           
 <div class="row">
  <div class="col-lg-6">
    <div class="input-group">
      <span class="input-group-btn">
        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
      </span>
      <input type="text" class="form-control" placeholder="Search for...">
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->
</div><!-- /.row -->  
 </div>
            <div id="prodTable" style="width:auto"></div>
      	<br>
 </div>
        <div id="insertProd" class="hide">     
            <h1>Insert Produkt!</h1>
 <form action='insert_product.php' id='imgForm' method='POST' enctype='multipart/form-data' >
     
<div class="fileinput fileinput-new" data-provides="fileinput">
               <div class="form-group">
            	<input type='text' name='prodName' id='prodName' class="form-control" placeholder="Enter Product Name">
                </div>
     
  <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
  <div>
    <span class="btn btn-default btn-file">
        <span class="fileinput-new">Select image</span>
        <span class="fileinput-exists">Change</span>
        <input type="file" name="prodimg">
    </span>
    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
    <input class="btn btn-default btn-file" type="submit" value="Submit Image" />
  </div>
</div> 
    
</form>            
            
        </div>    
 	</div>	       
        </div>
    <script src="../jq2/jquery.min.js"></script>
    <script src="../bootstrap3/js/bootstrap.min.js"></script>
    <script src="../jq2/bootstrapValidator.min.js" type="text/javascript" charset="utf-8" ></script> 
    <script src="../jasny/js/jasny-bootstrap.min.js"></script>
    <script src="../jasny/jquery.form.min.js"></script>
  	<script src="../jq2/bootstrap-datepicker.js" type="text/javascript" charset="utf-8"></script> 
 	<script src="../jq2/jquery.watable.js" type="text/javascript" charset="utf-8"></script>
   <script src="vadmin.js"></script>
  </body>
</html>