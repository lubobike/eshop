<?php
require_once('../core/vendor_info.php'); 
header("Content-type: application/json; charset=utf-8");

$vendor = new VendorInfo();
$vendor->loadFromWizardLink();

if(  $vendor->error == "" ) {
    if( $vendor->loginCheck()) {
        $ret = $vendor->getJsonLoginOk();
        setSession($ret->getData(),$vendor->emailenc);
        echo $ret->getJsonData();
    } else {
        echo $vendor->getJsonError();        
    }
} else {
    echo $vendor->getJsonError();
}