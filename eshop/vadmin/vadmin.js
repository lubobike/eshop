
$( document ).ready(function() {
    
        var sid="";

        $('#mainMenu').hide();
	$('#mainMenu').removeClass('hide');       
        $('#logdanger').hide();
	$('#logdanger').removeClass('hide');  
        $('#searchProd').hide();
	$('#searchProd').removeClass('hide');  
        $('#insertProd').hide();
	$('#insertProd').removeClass('hide'); 
        
        function showLoading()
	{
		$('#loadingModal').modal('show') ;	
	}
	function hideLoading()
	{
		$('#loadingModal').modal('hide') ;	
	}
	
	function showStatus(bigLabel, message)
	{
		$('#statusModalLabel').html(bigLabel);
		$('#modalmessage').html(message);
		$('#validatorModal').modal('show') ;
	} 
        
        $( document ).ajaxError(function(event, jqxhr, settings, exception) {
		
            hideLoading();
           var errorMessage = "N/A";
            try {
	        var json = $.parseJSON(jqxhr.responseText);
	        errorMessage = json.error;
	    } catch (e) { 
	    	errorMessage = serverNotResponding;
	    }
	    
            showStatus( jqerror+"!" , errorMessage );

	}); 
        
        $('#loginf').bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            onError: function (e, data) {
                $('#submit').prop('disabled', true);
            },
            onSuccess: function (e, data) {
                $('#submit').prop('disabled', false);
            },
             
            fields: {
                vemail: {
                    validators: {
                        emailAddress: {
                            message: invalidEmail
                        }
                    }
                },
                vpassw: {
                    validators: {
                        regexp: {

                            regexp: /^.{8,32}$/i,
                            message: shortLongPassword 
                        }
                    }
                } 
              
            }
        });       
    
 	$('#loginf').submit(function(event) {
		event.preventDefault();
                
                if($('#emailreg').val()== '' ){
                    showStatus( jqerror , emptyEmail );
                    return;
                }             
                if($('#passwf').val()== '' ){
                    showStatus( jqerror , emptyPassword );
                    return;
                } 
		showLoading();
 		var authurl =  "ajavenlogin.php?c="+c;		
		var frmJson = {json:JSON.stringify($('#loginf').serializeArray())};
		$.ajax({
                    url:  authurl ,
                    type: "POST",
                    dataType: "json",
		    data: frmJson,
		    success: function (data) {
		    	hideLoading();
                        if(data.error) {
                            showStatus( data.error , data.message );    
                        } else {
                            showStatus( data.label , data.message );                           
                            showSearch(data.message );
                            sid = data.data;
                        }
		    }
		});	
	});
        
        
             //An example with all options.
     var waTable = $('#prodTable').WATable({
        debug:false,                 //Prints some debug info to console
        pageSize: 10,                //Initial pagesize
        transition: 'slide',       //Type of transition when paging (bounce, fade, flip, rotate, scroll, slide).Requires https://github.com/daneden/animate.css.
        transitionDuration: 0.2,    //Duration of transition in seconds.
        filter: true,               //Show filter fields
        sorting: true,              //Enable sorting
        sortEmptyLast:true,         //Empty values will be shown last
        columnPicker: false,         //Show the columnPicker button
        pageSizes: [10, 20, 30],  			//Set custom pageSizes. Leave empty array to hide button.
        hidePagerOnEmpty: true,     //Removes the pager if data is empty.
        checkboxes: false,           //Make rows checkable. (Note. You need a column with the 'unique' property)
        checkAllToggle:true,        //Show the check-all toggle
        preFill: false,              //Initially fills the table with empty rows (as many as the pagesize).
        //url: '/someWebservice'    //Url to a webservice if not setting data manually as we do in this example
        //urlData: { report:1 }     //Any data you need to pass to the webservice
        //urlPost: true             //Use POST httpmethod to webservice. Default is GET.
        types: {                    //Following are some specific properties related to the data types
            string: {
                //filterTooltip: "Giggedi..."    //What to say in tooltip when hoovering filter fields. Set false to remove.
                //placeHolder: "Type here..."    //What to say in placeholder filter fields. Set false for empty.
            },
            number: {
                decimals: 6   //Sets decimal precision for float types
            },
            bool: {
                //filterTooltip: false
            },
            date: {
              utc: true,            //Show time as universal time, ie without timezones.
              //format: 'yy/dd/MM',   //The format. See all possible formats here http://arshaw.com/xdate/#Formatting.
              datePicker: true      //Requires "Datepicker for Bootstrap" plugin (http://www.eyecon.ro/bootstrap-datepicker).
            }
        },

        tableCreated: function(data) {    //Fires when the table is created / recreated. Use it if you want to manipulate the table in any way.
   //                  $(' tfoot ', this).hide();
        },
        rowClicked: function(data) {    //Fires when the table is created / recreated. Use it if you want to manipulate the table in any way.
  //      	golink2(glink,"i="+data.row.iid);
       }
    }).data('WATable');  //This step reaches into the html data property to get the actual WATable object. Important if you want a reference to it as we want here.

    function showSearch(message )
    {
        $('#logdiv').hide();
        $('#insertProd').hide();
        $('#mainMenu').show();   
        $('#searchProd').show();
        $('#searchPmenu').addClass("active");
        $('#addPmenu').removeClass("active");
        $('#editPmenu').removeClass("active");       
        $('#addFmenu').removeClass("active");        
        if(message)
            $('#logwelcome').html(message );
    }

    function showLoginPage(message)
    {
        $('#mainMenu').hide();       
        $('#insertProd').hide();
        $('#searchProd').hide();
        $('#logdiv').show();
        if(message)
            $('#logwelcome').html( message );
    }
    
    window.searchProduct = function()
    {
        showSearch();
    };
    
    window.addProduct = function()
    {
         $('#logdiv').hide();
        $('#mainMenu').show();   
        $('#searchProd').hide();       
        $('#insertProd').show();

        $('#searchPmenu').removeClass("active");
        $('#addPmenu').addClass("active");
        $('#editPmenu').removeClass("active");       
        $('#addFmenu').removeClass("active");        
    
    };
    window.editProfile = function()
    {
        $('#logdiv').hide();
        $('#mainMenu').show();   
        $('#searchProd').hide();       
        $('#insertProd').hide();

        $('#searchPmenu').removeClass("active");
        $('#addPmenu').removeClass("active");       
        $('#editPmenu').addClass("active");       
        $('#addFmenu').removeClass("active");       
    };
    window.feddbackForm = function()
    {
         $('#logdiv').hide();
        $('#mainMenu').show();   
        $('#searchProd').hide();       
        $('#insertProd').hide();

        $('#searchPmenu').removeClass("active");
        $('#addPmenu').removeClass("active");       
        $('#editPmenu').removeClass("active");       
        $('#addFmenu').addClass("active");        
    };
    
	window.vlogout = function()
	{
 	    var urllogout =  "ajaLogout.php?ci=" + sid;
		
		$.ajax({
			url:  urllogout ,
			type: "GET",
			success: function (data) {
				showLoginPage(data.message);
                                sid ="";
			}
		});	//*/		
	};


            var foptions = { 
                dataType:     'json', 
                success:    function(data) { 
                    showStatus( data.label , data.message ); 
                } 
            };            
            // bind 'myForm' and provide a simple callback function 
            $('#imgForm').ajaxForm(foptions);         
});


