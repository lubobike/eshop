<?php

require_once('../core/filters.php'); 
require_once('../core/BasicResponse.php'); 

header("Content-type: application/json; charset=utf-8");

if(checkSession()) {
    clearSession();
    $br = new BasicResponse("Ok", "Logout Successfull!");
    echo $br->getJsonData();
} else {
    $br = new BasicResponse("NOk", "Session Expired!");
    echo $br->getJsonData();
    
}

