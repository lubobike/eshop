

$( document ).ready(function() {

	$( document ).ajaxError(function(event, jqxhr, settings, exception) {
		
		var errorMessage = "N/A";
		
		try {
	        var json = $.parseJSON(jqxhr.responseText);
	        errorMessage = json.error;
	    } catch (e) { 
	    	errorMessage = "Server is Not Responding! Please Try Again Later.";
	    }		
		
		$('#loadingModal').modal('hide') ;	
		$('#modalmessage').html(errorMessage);
		$('#validatorModal').modal('show') ;
		
	});	    
    
     //An example with all options.
     var waTable = $('#chunkStamp').WATable({
        debug:false,                 //Prints some debug info to console
        pageSize: 10,                //Initial pagesize
        transition: 'slide',       //Type of transition when paging (bounce, fade, flip, rotate, scroll, slide).Requires https://github.com/daneden/animate.css.
        transitionDuration: 0.2,    //Duration of transition in seconds.
        filter: true,               //Show filter fields
        sorting: true,              //Enable sorting
        sortEmptyLast:true,         //Empty values will be shown last
        columnPicker: false,         //Show the columnPicker button
        pageSizes: [10, 20, 30],  			//Set custom pageSizes. Leave empty array to hide button.
        hidePagerOnEmpty: true,     //Removes the pager if data is empty.
        checkboxes: false,           //Make rows checkable. (Note. You need a column with the 'unique' property)
        checkAllToggle:true,        //Show the check-all toggle
        preFill: false,              //Initially fills the table with empty rows (as many as the pagesize).
        //url: '/someWebservice'    //Url to a webservice if not setting data manually as we do in this example
        //urlData: { report:1 }     //Any data you need to pass to the webservice
        //urlPost: true             //Use POST httpmethod to webservice. Default is GET.
        types: {                    //Following are some specific properties related to the data types
            string: {
                //filterTooltip: "Giggedi..."    //What to say in tooltip when hoovering filter fields. Set false to remove.
                //placeHolder: "Type here..."    //What to say in placeholder filter fields. Set false for empty.
            },
            number: {
                decimals: 6   //Sets decimal precision for float types
            },
            bool: {
                //filterTooltip: false
            },
            date: {
              utc: true,            //Show time as universal time, ie without timezones.
              //format: 'yy/dd/MM',   //The format. See all possible formats here http://arshaw.com/xdate/#Formatting.
              datePicker: true      //Requires "Datepicker for Bootstrap" plugin (http://www.eyecon.ro/bootstrap-datepicker).
            }
        },

        tableCreated: function(data) {    //Fires when the table is created / recreated. Use it if you want to manipulate the table in any way.
   //                  $(' tfoot ', this).hide();
        },
        rowClicked: function(data) {    //Fires when the table is created / recreated. Use it if you want to manipulate the table in any way.
  //      	golink2(glink,"i="+data.row.iid);
       }
    }).data('WATable');  //This step reaches into the html data property to get the actual WATable object. Important if you want a reference to it as we want here.

	function refreshDiagnosticTable(  )
	{
            $('#loadingModal').modal('show') ;	
    	
            $.ajax({
                    url: "jsonRegTable.php",
                            type: "POST",
                            headers: {   "dojo-head": "1"  },
                            success: function (data) {
                            waTable.setData(data);
                            $('#loadingModal').modal('hide') ;	
                        }
            });	
        
	}	
	
	refreshDiagnosticTable();
	setInterval(refreshDiagnosticTable, 60000);	
	
	$('#refreshnow').click( function() {
		refreshDiagnosticTable();
	});
	
	 
});    