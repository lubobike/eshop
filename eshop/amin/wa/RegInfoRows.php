<?php

/**
 * Description of RegInfoRows
 *
 * @author lubo
 */
class RegInfoRows {
    
    public $email ;
    public $createDate;
    public $ipaddress; 
    public $verified;
    public $lang;
    public $expirationDate;
    public $lev;
    
    function __construct($em, $crDt, $ip, $v, $la, $ex, $lv) {
        
        $this->email = $em;
        $this->createDate = $crDt;
        $this->ipaddress = $ip;
        $this->verified = $v;
        $this->lang = $la;
        $this->expirationDate = $ex;
        $this->lev = $lv;
        
    }
}
