<?php

require_once 'RegInfoRows.php';
require_once 'RegInfoCols.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RegInfoTable
 *
 * @author lubo
 */
class RegInfoTable {
    
	public $cols ;
	public  $rows  ;
        
        private $count=0;
        
        function __construct()
        {
            $this->cols = new RegInfoCols();
            $this->rows =  Array();
        }
        
        function addRow($em , $dt, $ip, $v, $la, $ex, $lev)
        {
            $this->rows[$this->count++] = new RegInfoRows($em,$dt, $ip, $v, $la, $ex, $lev);
        }

}
