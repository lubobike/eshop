<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RegInfoCols
 *
 * @author lubo
 */

require_once 'WaCol.php';

class RegInfoCols {

	public  $email ;
	public  $createDate ;
        public  $ipaddress;
        public $verified;
        public $lang;
        public $expirationDate;
        public $lev;
 
        function __construct()
        {
            $this->email = new WaCol(1,"string", "Email");
            $this->createDate = new WaCol(2,"date", "Create Date"); 
            $this->ipaddress = new WaCol(3,"string", "IP Address");
            $this->verified = new WaCol(4,"bool", "Verified");
            $this->lang = new WaCol(5,"string", "Language");
            $this->expirationDate = new WaCol(6,"date", "Expiration Date");
            $this->lev = new WaCol(7,"number", "Level");
        }
}
