<?php

require_once 'wa/RegInfoTable.php';
require_once('../core/db/BaseLoginInfo.php'); 

$conn = BaseLoginInfo::createConnection();

header("Content-type: application/json; charset=utf-8");
$ret = new RegInfoTable();

if ($stmt = $conn->prepare("SELECT v_email , v_createDate , v_ip_address, v_verified, v_lang, v_expiration_date, v_level FROM vendor_reg ORDER BY v_createDate DESC;"))
{
    $stmt->execute();
    $stmt->bind_result($v_email, $v_createDate, $v_ip_address, $v_verified, $v_lang, $v_expiration_date, $v_level );
    while ($stmt->fetch()) {
        $ret->addRow($v_email, $v_createDate, $v_ip_address, $v_verified, $v_lang, $v_expiration_date, $v_level ) ;// */
    }
    
} else { echo "error select (" . $conn->errno . ") " . $conn->error;  }

$conn->close();

echo json_encode($ret);
