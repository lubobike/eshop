<!DOCTYPE html>
<html lang="sk">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Lubo Sladok">
 
    <title>Vendor Registration Info </title>
    <link href="../bootstrap3/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../jq2/datepicker.css">
    <link rel="stylesheet" href="../jq2/watable.css">
    
 	<script src="../jq2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="../bootstrap3/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
 	<script src="../jq2/bootstrap-datepicker.js" type="text/javascript" charset="utf-8"></script> 
 	<script src="../jq2/jquery.watable.js" type="text/javascript" charset="utf-8"></script>
	<script src="regTable.js" type="text/javascript" charset="utf-8"></script>
  </head>

  <body role="document">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
         <a class="navbar-brand" href="#">Home</a> 
        </div>
         <div class="navbar-header">
         <a class="navbar-brand" href="#">Product</a> 
        </div>         
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
             <li > <a  href="#">Logout</a></li>           
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

     <div class="container" role="main">
      
      <div class="page-header">
        <h3>List of Registered Users</h3>
      </div>     
      
     <div class="container">
     
<!-- Modal -->
<div class="modal" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" >.......Please Wait........</h4>
      </div>
      <div class="modal-body" >
       <img src="../img/ajax-loader.gif"> 
      </div>
    </div>
  </div>
</div>	     
     
<!-- Modal -->
<div class="modal" id="validatorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Error!</h4>
      </div>
      <div class="modal-body" id="modalmessage">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>	
			  

      	<div id="chunkStamp" style="width:auto"></div>
      	<br>
      	
      	<button class="btn btn-primary" type="button" id="refreshnow">Refresh Now</button>
 	 </div>
      
      
    </div>
 
  </body>
</html>
