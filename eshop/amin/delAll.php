<?php

require_once('../core/db/BaseLoginInfo.php'); 

$conn = BaseLoginInfo::createConnection();


if ($stmt = $conn->prepare("DELETE FROM vendor_reg ")){
    $stmt->execute();
} else { echo "error delete (" . $conn->errno . ") " . $conn->error;  }

if ($stmt = $conn->prepare("DELETE FROM ven_products ")){
    $stmt->execute();
} else { echo "error delete (" . $conn->errno . ") " . $conn->error;  }

$conn->commit();
 
$conn->close();
